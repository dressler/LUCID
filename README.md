# LUCID Land Use Competition In Drylands model
--------------------------------------------------------------------------------

## About

The Land Use Competition in Drylands (LUCID) model is a joint effort of the Helmholtz Centre for Environmental Research - UFZ and the International Livestock Research Institute (ILRI) to use simulation modeling as a tool for supporting land use planning processes, and to address decision-making around competition among alternative land uses in pastoral and agro-pastoral drylands.

## Version Changelog

### 2018-01-05 (v.0.91)

- Household consumption: working implementation based on milk production, crop harvest and herd offtake; parameterization not finished yet
- Population growth: implemented fixed annual growth rate (prop-growth-rate)
- Crop area allocation: spatial explicit allocation of cultivated area for each household

### 2019-04-30

- moved reporters to new include file "LUCID_Reporters.nls"
- changed names of some reporters for higher clarity and better compatibility with R (i.e. changed hyphen to underscore)
- added log-messages? switch to only print log messages in the console if needed
- changed household consumption calculation
- added some new plots  

### 2019-07-07 (v.0.92)

- model cleanup and bugfixes! --> all the work done by Felix Jäger (see detailed comments in documentation/Dokumentation_LUCID.docx)
    - deleted all unused functions and variables
    - herd size only saved as herd-size-total for milk herds --> individual herd sizes for foora and milk can be calculated with new functions "herd-size-milk" and "herd-size-foora"
    - added function "herd-size-check" to check if herd sizes are < 0 or > than maximum herd size
    - setup function simplified
    - removed code repetitions (specifically doubled while loops) in function "cultivated-area-increase-2"
    - in go function, all loops will be done only for milk herds
    - many global variables are now only reporters in "LUCID_Reporters.nls"

### 2019-07-24

- repository structure cleaned up:
    - all documentation files (model parameter files, implementation status, etc.) moved to "documentation" subfolder
    - all old model versions deleted (backup on UFZ home drive)
    - removed model version (v.0.91) from file name --> "LUCID.nlogo"
