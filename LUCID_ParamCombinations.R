# Baseline
x_steps = 400 # = 100 years

# Cultivation
cultivated_soil_types = c("none", "bottomland", "upland", "all")
cultivation_on_enclosures = c(TRUE, FALSE)
cultivated_area_max_per_hh = c(2,4,6,8)
cultivated_area_delta_per_hh = seq(0.1,1,by=0.1)

cultivation_params = expand_grid(cultivated_soil_types, cultivation_on_enclosures, 
                                 cultivated_area_max_per_hh, cultivated_area_delta_per_hh)
# --> 320 combinations

# Climatic conditions
rain_mean = seq(500,900,by=100)
rain_sd = seq(100,300,by=100)

climatic_params = expand_grid(rain_mean, rain_sd)
# --> 45 combinations

# Ecological conditions
mr = seq(0,0.2,by=0.1)
w = seq(0.6,0.8,by=0.1)
upland_growth_rate_factor = seq(0.25,1,by=0.25)
upland_reserve_max_factor = seq(0.25,1,by=0.25)
enclosure_size = seq(40,100,by=20)
#
ecological_params = expand_grid(mr, w, upland_growth_rate_factor, upland_reserve_max_factor, enclosure_size)
# --> 576 combinations

# Household & livestock conditions
agents_per_settlement = seq(60,100,by=10)
reproduction_rate = c(0.2,0.3,0.4)

household_params = expand_grid(agents_per_settlement, reproduction_rate)
# --> 21 combinations

nrow(cultivation_params) * nrow(climatic_params) * nrow(ecological_params) * nrow(household_params)
