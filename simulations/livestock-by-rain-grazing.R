setwd("c:/Users/hasen/Dropbox/YabelloModel/netlogo-model/simulations/")

livestock <- read.csv(file = "yabello-rangeland-use livestock-by-rain-grazing-table.csv", skip = 6)
livestock.sub <- subset(livestock,X.step. == 400, select = c(rain.mean, mean.heard.size, yearly.intake))
livestock.sub$rain.mean <- as.factor(livestock.sub$rain.mean)

library(ggplot2)
p <- ggplot(livestock.sub, aes(rain.mean, mean.heard.size))
p + geom_boxplot() + facet_wrap(~ yearly.intake, 
                                labeller = "label_both")
