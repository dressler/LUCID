;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LUCID Land Use Competition In Drylands model ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; plotting, output and some helper functions are moved to external source files
__includes [ "LUCID_HelperProcedures.nls" "LUCID_PlottingProcedures.nls" "LUCID_Reporters.nls" ]
;extensions [ vid ] ;bitmap ]

globals
[
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; model parameters
  ;; variable names that are commented are defined via the interface, only noted here for completeness
  ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

  ;; general factors ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ticks-per-year                                ; how many ticks are there per year?
  model_random_seed
  ;nlrx?                                        ; indicates whether analysis via R nlrx package are being run
                                                ; in this case, no random seed is set by the model, as it is provided by the xml file

  ;; seasons and rain ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  season                                        ; current season, i.e. 0, 1, 2 or 3
  rainy-season?                                 ; indicates if current season is a rainy season or not
  season_mean_weights                           ; seasonal weights for mean rainfall
  season_sd_weights                             ; seasonal weights for standard deviation of rainfall
  rainfall-list                                 ; list of global rainfall values with length = x-steps
  global-rainfall                               ; current global rainfall

  ;; household consumption values - currently constant for all hh ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  kcal_per_capita_day                           ; consumption need per capita and day, van Wijk assumes 2500 kcal / day
  members-per-household                         ; household size, we currently assume each household consists of 6 persons
                                                ; (is roughly the mean hh size of the IBLI Borana data )
  consumption-needed                            ; total consumption needed per hh and year (currently constant)
  maize_calories_per_kg                         ; maize calories per kg
  milk_calories_per_kg                          ; milk calories per kg
  milk-production-per-day                       ; average production of milk per cattle [l/day]
  maize_price_kg                                ; maize price in ETB per kg
  cattle_price_tlu                              ; cattle price in ETB per head

  ;; herd movement radii for different herd types and seasons ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  radius-dry-foora                              ; movement radius for foora herds in dry season
  radius-rainy-foora                            ; movement radius for foora herds in rainy season
  radius-dry-milk                               ; movement radius for milk herds in dry season
  radius-rainy-milk                             ; movement radius for milk herds in rainy season
  moving-radius?                                ; should movement radius be considered for herd relocation?

  ;; longterm variables for plotting, updated in longterms-update-tick-end ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; for herd size
  herd_size_total_mean_longterm                 ; longterm mean of total herd size (milk + foora)
  herd_size_milk_mean_longterm                  ; longterm mean of milk herd size
  herd_size_foora_mean_longterm                 ; longterm mean of foora herd size
  ;; for reserve and green biomass
  reserve_enclosure_longterm                    ; longterm mean of reserve biomass on enclosures
  reserve_warra_longterm                        ; longterm mean of reserve biomass on warra pastures
  reserve_foora_longterm                        ; longterm mean of reserve biomass on foora oastures
  green_enclosure_longterm                      ; longterm mean of green biomass on enclosures
  green_warra_longterm                          ; longterm mean of green biomass on warra pasures
  green_foora_longterm                          ; longterm mean of green biomass on foora pastures
  ;; for consumption
  milk_production_mean_longterm                 ; longterm mean of household milk production
  crop_harvest_mean_longterm                    ; longterm mean of household crop production (i.e. harvest)
  herd_offtake_mean_longterm                    ; longterm mean of household herd offtake
  hh_regained_herd
  ;; cultivation increase/decrease counter
  poor-hh-increase-cultivation
  rich-hh-increase-cultivation
  medium-hh-decrease-cultivation
  ;; offtake
  herd-offtake-counter
  grazing-destock-counter

  ;; only needed for video recording visualization (needs vid extension)
  drawn?
  t0
  eraser
  vid-step
]

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MODEL BREEDS ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
breed [ settlements settlement ]
breed [ herds herd ]
;; the following two breeds are only needed for visualisation purposes
breed [ crop-dummies crop-dummy ] ; for view-crop-shares
breed [ clocks clock ] ; for view-clock

;; settlements only define a "home" for the households (herds) and define the radius for herd movement and crop cultivation ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
settlements-own
[
  ;; accessible pastures
  accessible-patches-dry-foora                  ;
  accessible-patches-dry-milk
  accessible-patches-rainy-foora
  accessible-patches-rainy-milk
  ;; possible cultivated patches
  possible-cultivated-patches
]

;; patches
patches-own
[
  ;general state of the patch
  soil-type                                     ; patch soil type, one of "bottomland", "upland", "none"
  land-use                                      ; patch land use type, one of "cropland", "enclosure", "barren", "foora", "warra"
  land-use-old                                  ; temporary variable to save land use when patches are turned into cropland
  patch-cultivated-area                         ; amount of current cultivated area on the patch

  ;biomass variables
  green-biomass                                 ; current green biomass [kg]
  green-biomass-before-grazing
  reserve-biomass                               ; current reserve biomass [kg]
  p-reserve-max                                 ; patch dependent adjustment factor of reserve max value, depends on soil type
  p-growth-rate                                 ; patch dependent adjustment factor of reserve growth rate, depends on soil type
  p-crop-max                                    ; patch dependent adjustment factor of max. crop yield, depends on soil type
                                                ; --> not used anymore!

  ;; local rain
  local-rainfall-list                           ; list of local rainfall values with length = x-steps
  local-rainfall                                ; current local rainfall

  ;; auxiliary variables
  dist                                          ; to calculate the accessible patches if moving-radius? = true
  height                                        ; for clustering soil types
  height2                                       ; for clustering soil types
]

;; herds
herds-own
[
  ;; herd type and sizes ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  foora?                                        ; indicator for herd type - true: foora herd, false: milk-herd
  herd-size-total                               ; total herd size = sum of foora + milk-herd, stored only at milk herd
  herd-size                                     ; single herd sizes milk/foora, only temporarily used for grazing and movement in go
  home-settlement                               ; variable type: settlement
  associated-herd                               ; the associated milk/foora herd

  ;; the remaining variables are for *households* and are stored at the milk herd ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
  ;; cultivation
  land                                          ; area of cultivated land, varibale type : float, unit: ha per hh
  cultivated-patches                            ; set of patches on which the hh has cultivated land
  cultivated-patches-and-areas                  ; nested list that stores both cultivated patches and amount of cultivated land on that patch
  indiv-reproduction-rate                       ; individual herd reproduction rate, dependent on herd size
  ;; household consumption
  herd-offtake-conversion-factor                ; not needed anymore, remove in future
  household-food-security                       ; measure that keeps track of current state of consumption
  ;; variables that store the yearly amounts of the different sources of consumption of each household
  household-crop-harvest                        ; amount of harvest from crops in kg - calculated for each season
                                                ;  (but will most likely be succesful only in rainy seasons)
  household-milk-production                     ; production of milk in l, proportional to milk herd size of the household
  household-herd-offtake                        ; in TLU, calculated as consumption deficit that is not covered by milk and crop harvest
                                                ;  (subsumes sold and slaughtered animals)
  household-milk-consumption                    ; milk production converted to kcal
  household-crop-consumption                    ; crop harvest converted to kcal
  household-herd-consumption                    ; consumption that can be covered by livestock
                                                ;  (in kcal - approximated by conversion of herd offtake to maize purchase) - if herd size
                                                ;  is sufficient, this will be consumption-needed-remainder, otherwise it will be lower

  ;; count the years that households do not reach their consumption needs / food security
  years-below-consumption-needed
  household-food-security-years-below-one-total
  household-food-security-years-below-one-longest
  household-food-security-years-below-one-current
]

clocks-own
[
  is-clock-arrow?                               ; for view-clock? true
]
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; MODEL SETUP ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to setup
  ;; clear everything
  ca

  ;; initialize some global variables
  ;set nlrx? false ; standard case - has to be specified as true in all analysis run via R
  set ticks-per-year 4
  set rainfall-list []
  set drawn? false
  set-default-shape crop-dummies "croplands12"
  set t0 true
  set rainy-season? false
  set poor-hh-increase-cultivation 0
  set rich-hh-increase-cultivation 0
  set medium-hh-decrease-cultivation 0
  set grazing-destock-counter 0
  set herd-offtake-counter 0
  set hh_regained_herd 0

  ;; herd movement
  set radius-dry-foora 15
  set radius-rainy-foora 25
  set radius-dry-milk 15
  set radius-rainy-milk 23
  set moving-radius? true

  ;; Household consumption
  ; 1. total consumption needed is the sum of daily calories needed per hh member for the whole year
  set kcal_per_capita_day 2500 ; van Wijk assumes 2500 kcal / day
  set members-per-household 6 ; we assume an average household consists of 6 persons (is roughly the mean hh size of the IBLI Borana data )
  set consumption-needed kcal_per_capita_day * members-per-household * hh-per-agent * 365
  ;; 2. define caloric content of different food items and food prices
  set maize_calories_per_kg 3650    ; USDA statistics
  set milk_calories_per_kg 620      ; USDA statistics
  set milk-production-per-day 1.5   ; average amount of milk produced per cow [l/day]
  set maize_price_kg 5.5            ; ETB , based on average monthly prices 2013-2017, World Food Programme
  set cattle_price_tlu 4600         ; ETB, average price 2006-2015, based on Ethipian Livestock Market Information System (ELMIS), calculated by Feed the Future / USAID

  reset-ticks

  if (log-messages?) [ output-print "new simulation" ]

  ;; set random seed
  ifelse fixed-seed?
  [
    random-seed seed
  ]
  [
    if ( not nlrx?) [
      set model_random_seed new-seed
      random-seed model_random_seed
    ]
  ]

  ;; landscape initialisation (foora/warra, barrens, settlements) according to Lance's drawing
  if (log-messages?) [ output-print "landscape initialization" ]
  setup-landscape

  ;; soil type and patch-cultivated-area setup
  if (log-messages?) [ output-print "soiltype setup" ]
  setup-patch-soil-types

  ;; global and local rainfall list and biomass variables setup
  if (log-messages?) [ output-print "rainfall and biomass setup" ]
  setup-patch-rainfall-and-vegetation

  ;; enclosure setup
  if (log-messages?) [ output-print "enclosures setup" ]
  setup-enclosures

  ;; herds setup
  if (log-messages?) [ output-print "herds setup" ]
  setup-herds

  ;; accessible pastures setup
  settlements-set-accessible-patches

  ;; possible cultivated patches setup
  settlements-set-possible-cultivated-patches

  ;; update the longterm variables initially after setup has completed for correct plotting
  globals-update-tick-begin
  globals-update-tick-end

  ;; if save-indiv-hh?, then individual measures per household are saved to a .csv file
  ;; this sets up the file header
  if ( save-indiv-hh? ) [
    output_hh_herdsize
  ]

  ;; visualization and plotting
  view-herds
  view-patches
  view-settlements
  view-years

  ;; for video output (needs vid extension)
  ;if vid:recorder-status = "recording"
  ;[
  ;  vid:record-view
  ;  set vid-step 0
  ;]
  update-plots
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; GO PROCEDURE ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to go
  ifelse ( ticks < x-steps )
  [
    tick-advance 1

    ;; update season and current rain
    globals-update-tick-begin

    ;; BIOMASS GROWTH (every season)
    biomass-growth-seasonal

    ;; accessible pastures update
    settlements-set-accessible-patches

    ;; POPULATION GROWTH AND HH RETURN TO PASTORALISM (once per year)
    if ( season = 0 )
    [
      settlements-increase-households
      households-regain-herd
    ]

    ;; operations for every agent
    ask herds with [not foora?]
    [
      ;; HERD REPRODUCTION (once per year)
      if season = 0
      [
        herds-reproduction
      ]
      ;; HERD MOVEMENT AND GRAZING (every season)
      ;; for movement and grazing the single herd sizes of milk and foora herd are used
      ;; first the milk herds
      herd-size-individual-update
      herds-move
      herds-graze-destock
      ;; then the associated foora herds
      ask associated-herd
      [
        herds-move
        herds-graze-destock
      ]
      ;; update herd-size-total from single herd sizes
      herd-size-total-update

      ;; HOUSEHOLD CONSUMPTION AND CULTIVATED AREA CHANGE (once per year)
      if ( season = 0 )
      [
        ;; calculation of household consumption with possible herd offtake or death
        if ( households-consumption? )
        [
          households-consumption
        ]
        ;; adjustment of cultivated area in case cultivation is allowed
        if ( not cultivated-area-constant? and floor ( ticks / 4 ) + 1 >= cultivation-start-year and cultivated-soil-types != "none" )
        [
          households-increase-cultivated-area
        ]
      ]
    ]

    ;; update longterm variables
    globals-update-tick-end

    ;; if save-indiv-hh?, then individual measures per household are saved to a .csv file
    if ( save-indiv-hh? ) [
      output_hh_herdsize
    ]

    ;; visualization and plotting
    update-plots
    view-herds
    view-patches
    view-cropland
    view-settlements
    view-years
  ]
  [
    if ( export-plots? )
    [
      let d remove ":" remove " " date-and-time
      let timestamp ( word substring d 0 6 "_" substring d 10 12 "_" substring d 12 23 )
      export-plot "rain" ( word "simulations/csv/rain_" timestamp ".csv" )
      export-plot "mean herdsize per hh" ( word "simulations/csv/herdsizes_" timestamp ".csv" )
      export-plot "Herds and Cultivation" ( word "simulations/csv/cultivation_" timestamp ".csv" )
      export-plot "green biomass by pasture type" ( word "simulations/csv/greenBiomass_landuse_" timestamp ".csv" )
      export-plot "green-biomass by soil and pasture type" ( word "simulations/csv/greenBiomass_soiltype_" timestamp ".csv" )
      export-plot "reserve biomass by pasture type" ( word "simulations/csv/reserveBiomass_landuse_" timestamp ".csv" )
      export-plot "total cropland area" ( word "simulations/csv/cropland_area_" timestamp ".csv" )
    ]
    stop
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SETUP SUBPROCEDURES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to setup-landscape
  ;fixed spatial arrangement of specific landscape elements
  ;set foora/warra: lower part of the landscape is warra, upper part is foora
  landscape-init-square 0 24 20 49 "foora"
  landscape-init-square 0 24 0 19 "warra"

  ;barrens
  landscape-init-square 0 7 32 32 "barren" ;left horizontal
  landscape-init-square 8 8 11 32 "barren" ;left vertical
  landscape-init-square-by-length 10 0 0 4 "barren" ;bottom,left
  landscape-init-square-by-length 10 3 0 0 "barren" ;bottom,left

  ;settlements
  create-settlements 1 [set xcor 8 set ycor 5] ;left
  create-settlements 1 [set xcor 16 set ycor 0] ;middle
  create-settlements 1 [set xcor 23 set ycor 5] ;right
end

to setup-patch-soil-types
  ; procedure to set up patches with bottomland/upland/none as soil type
  ; percentages are defined via bottomlands-warra, bottomlands-foora, uplands-warra, uplands-foora
  ; bottomlands-none = 1 - (bottomlands-warra + bottomlands-foora) (same for uplands)

  ; initialize patch-cultivated-area and auxiliary variables
  ask patches
  [
    set height 0
    set height2 0
    set patch-cultivated-area 0
  ]

  ; calculate number of bottomland/none-patches in foora and warra region
  let bottom_warra precision (bottomlands-warra * count patches with [ land-use = "warra" ]) 0
  let bottom_foora precision (bottomlands-foora * count patches with [ land-use = "foora" ]) 0
  let none_warra precision ((1 - (bottomlands-warra + uplands-warra)) * count patches with [ land-use = "warra" ]) 0
  let none_foora precision ((1 - (bottomlands-foora + uplands-foora)) * count patches with [ land-use = "foora" ]) 0

  ; set up "bottomland" hotspots
  let patches-list-warra n-of 10 patches with [ land-use = "warra" and distance min-one-of settlements [ distance myself ] < 10 ]
  let patches-list-foora n-of 5 patches with [ land-use = "foora" ]
  ask patches-list-warra
  [
    set height -100
  ]
  ask patches-list-foora
  [
    set height -100
  ]

  ; diffuse height for clustering
  repeat 5 [ diffuse height 1 ]

  ;set up "bottomland"-patches
  ask min-n-of bottom_warra patches with [ land-use = "warra" ] [ height ] [ set soil-type "bottomland" ]
  ask min-n-of bottom_foora patches with [ land-use = "foora" ] [ height ] [ set soil-type "bottomland" ]

  ; set up "none" hotspots
  ask n-of 10 patches with [ land-use = "warra" and soil-type != "bottomland" ]
  [
    set height2 -100
  ]
  ask n-of 10 patches with [ land-use = "foora" and soil-type != "bottomland" ]
  [
    set height2 -100
  ]

  ; diffuse height2 for clustering
  repeat 5 [ diffuse height2 1 ]

  ; set up "none"-patches
  ask min-n-of none_warra patches with [ land-use = "warra" and soil-type != "bottomland" ] [ height2 ] [ set soil-type "none" ]
  ask min-n-of none_foora patches with [ land-use = "foora" and soil-type != "bottomland" ] [ height2 ] [ set soil-type "none" ]

  ; set up "upland"-patches
  ask patches with [ soil-type != "bottomland" and soil-type != "none" ] [ set soil-type "upland" ]

  if (log-messages?) [
    type ( word "bottomlands-warra " (count patches with [ land-use = "warra" and soil-type = "bottomland" ] / count patches with [ land-use = "warra"]) " / " bottomlands-warra " | ")
    type ( word "uplands-warra " (count patches with [ land-use = "warra" and soil-type = "upland" ] / count patches with [ land-use = "warra"]) " / " uplands-warra " | ")
    print ( word "none-warra " (count patches with [ land-use = "warra" and soil-type = "none" ] / count patches with [ land-use = "warra"]) " / " (1 - bottomlands-warra - uplands-warra ) )
    type ( word "bottomlands-foora " (count patches with [ land-use = "foora" and soil-type = "bottomland" ] / count patches with [ land-use = "foora"]) " / " bottomlands-foora " | ")
    type ( word "uplands-foora " (count patches with [ land-use = "foora" and soil-type = "upland" ] / count patches with [ land-use = "foora"]) " / " uplands-foora " | ")
    print ( word "none-foora " (count patches with [ land-use = "foora" and soil-type = "none" ] / count patches with [ land-use = "foora"]) " / " (1 - bottomlands-foora - uplands-foora ) )
  ]
end

to setup-patch-rainfall-and-vegetation
  ;; seasonal weights for biomass / rainfall (LR - SD - SR - LD)
  ;; Toth:
  ;;  mean 0.5 0.05 0.35 0.1
  ;; Homann 2005:
  ;;  mean 0.51 0.05 0.29 0.15
  ;; Homann 2005, Berhanu and Beyene 2015
  ;;  CV: 0.45 0.35 0.58 0.35 ; not sure if this is in correct season order
  ;; CHIRPS data 1981-2018:
  ;;  mean 0.52 0.08 0.30 0.10 (x 670 mm)
  ;;  CV   0.28 0.45 0.34 0.49 (x 220 mm)
  set season_mean_weights (list 0.52 0.08 0.30 0.10)
  set season_sd_weights (list 0.44 0.11 0.30 0.15)

  ; set up the global rainfall list, rain falls log-normally distributed
  let t 0
  let sd-rain 0
  repeat x-steps + 4 ;; simulate 4 seasons more so that household consumption for last tick can be calculated correctly
  [
    ;set sd-rain item ( t mod 4 ) season-weights * ifelse-value weighted-sd? [ rain-mean * item ( t mod 4 ) season-sd ][ rain-sd ]
    let rain_mean_season ( item ( t mod 4 ) season_mean_weights * rain-mean )
    let rain_sd_season ( item ( t mod 4 ) season_sd_weights * rain-sd )
    set rainfall-list lput ( calc-log-normal-rain rain_mean_season rain_sd_season ) rainfall-list
    set t t + 1
  ]

  let group-size 5 ; edge length of squares of patches that get the same local rainfall
  let temp-x-cor 0
  let temp-y-cor 0

  ; go through every (group-size x group-size)-square
  while [ temp-x-cor < max-pxcor ]
  [
    set temp-y-cor 0
    while [ temp-y-cor < max-pycor ]
    [
      ; create a temporary local rainfall list : the global rainfall list with normally distributed "errors" added
      let temp-local-rainfall-list []
      set t 0
      repeat x-steps + 4
      [
        set temp-local-rainfall-list lput (max list (random-normal 0 rain-spatial-sd + item t rainfall-list) 0 ) temp-local-rainfall-list
        set t t + 1
      ]

      ; go through all patches in the square
      ask patches with [ pxcor >= temp-x-cor and pxcor < temp-x-cor + group-size and pycor >= temp-y-cor and pycor < temp-y-cor + group-size ]
      [
        ; set up local-rainfall-list as global rainfall list or temporary local rainfall list
        ifelse global-rainfall? [
        set local-rainfall-list rainfall-list
        ]
        [
         set local-rainfall-list temp-local-rainfall-list
        ]

        ; initialize biomass variables

        if land-use = "barren" [set soil-type "none"]
        ifelse soil-type = "bottomland"
        [
          set p-growth-rate w           ; growth rate from interface
          set p-reserve-max reserve-max ; reserve max from interface
          set p-crop-max 1              ; standard
        ]
        [
          ifelse soil-type = "upland"
          [
            set p-growth-rate upland-growth-rate-factor * w           ; uplands have worse growth rate
            set p-reserve-max upland-reserve-max-factor * reserve-max ; reduced biomass capacity on uplands
            set p-crop-max upland-crop-max-factor                     ; reduced max. harvest on uplands
          ]
          [
            ; on none-pastures no forage grows
            set p-reserve-max 0
            set p-growth-rate 0
            set p-crop-max 0
          ]
        ]
        set green-biomass green-biomass-init
        set green-biomass-before-grazing green-biomass
        set reserve-biomass reserve-biomass-init
      ]
      set temp-y-cor temp-y-cor + group-size
    ]
    set temp-x-cor temp-x-cor + group-size
  ]
end

to setup-enclosures
  ; enclosures are connected patch groups of random size, total enclosure size is given as enclosure-size

  let enclosure-size-remaining enclosure-size

  ; in every step one enclosure is created until total enclosure size is reached
  while [ enclosure-size-remaining > 0 ]
  [
    ; set up first patch of the new enclosure
    let relevant-patch ifelse-value ( count patches with [ land-use = "enclosure" ] = 0 )
    [
      one-of patches with [ land-use = "warra" and soil-type != "none" ]
    ]
    [
      one-of patches with [ land-use = "warra" and soil-type != "none" and distance min-one-of patches with [ land-use = "enclosure" ] [ distance myself ]  > enclosure-distance ]
    ]

    ask relevant-patch
    [
      set land-use "enclosure" ; update land use

      ; determine enclosure size (limited by number of neighbors with enclosure potential)
      let neighbors-warra count neighbors with [ land-use = "warra" and soil-type != "none" ]
      let new-enclosure-area random min ( list 7 ( enclosure-size-remaining - 1 ) ( neighbors-warra - 1 ) ) + 1
      set enclosure-size-remaining enclosure-size-remaining - ( new-enclosure-area + 1 )

      let curr-patch self

      ; in every step add new patches to the enclosure, new patch becomes current patch
        while [ new-enclosure-area > 0 ]
        [
          ask curr-patch
          [
            carefully
            [
             ; throw a coin
              ifelse ( random-float 1 > 0.5 )
              [
              ; new patch chosen as neighbor of current patch and first patch
                ask min-one-of neighbors with [ land-use = "warra" and soil-type != "none" ] [ distance relevant-patch ]
                [
                  set land-use "enclosure" ;update land use
                  set curr-patch self ;update current patch
                ]
              ]
              [
              ; new patch chosen only as random neighbor of current patch
                ask one-of neighbors with [ land-use = "warra" and soil-type != "none" ]
                [
                  set land-use "enclosure" ;update land use
                  set curr-patch self ;update current patch
                ]
              ]
            ]
            [
              set curr-patch relevant-patch
            ]
          ]
          set new-enclosure-area new-enclosure-area - 1
        ]
    ]
  ]
end

to setup-herds
  ;herds are created per settlement
  ask settlements
  [
    let this-settlement self
    ;create a number of milk-herd agents
    hatch-herds agents-per-settlement  ; set via the interface
    [
      set foora? false
      ; setup initial values
      set cultivated-patches no-patches
      set cultivated-patches-and-areas []
      set years-below-consumption-needed 0

      set land 0
      with-local-randomness [
        ; set herd-size-total randomly between herdsize-init-max and min
        set herd-size-total herd-size-check ( ( herdsize-init-min + random ( herdsize-init-max + 1 - herdsize-init-min ) ) * hh-per-agent)
      ]
      set home-settlement this-settlement
      set shape "cow"

      ; to associate milk and foora herds to each other
      let this-milk-herd self
      let this-foora-herd nobody

      ; create foora-herds ; inherits all variables from parent
      hatch-herds 1
      [
        set foora? true
        set associated-herd this-milk-herd
        set this-foora-herd self
        set herd-size-total -1
      ]
      set associated-herd this-foora-herd

      set household-food-security 0
      set household-food-security-years-below-one-total 0
      set household-food-security-years-below-one-longest 0
      set household-food-security-years-below-one-current 0
    ]
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; BIOPHYSICAL PROCESSES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; Rainfall
to-report calc-log-normal-rain [ _mean _sd ]
  ;; Draw a random rainfall value out of a log-normal distribution with given mean and sd
  ;; 1. Calculate sigma and mu out of rain-mean and rain-std
  let sigma sqrt ( ln ( ( _sd ^ 2 ) / ( _mean ^ 2 ) + 1 ) )
  let mu ln _mean - 0.5 * ( sigma ^ 2 )
  ;; 2. Draw a random number x ~ N(mu, sigma)
  let x random-normal mu sigma
  ;; 3. Transform to LogNorm random value by calculating exp(x)
  report exp x
end

;; Vegetation growth
;; PATCH PROCEDURE
to-report biomass-green-growth [ green-biomass-t-1 rain-t reserve-biomass-t-1]
  ;; calculate increase in green-biomass value
  report min list (lambda * reserve-biomass-t-1)  ( (1 - m.g ) * green-biomass-t-1 + rain-t * rue * reserve-biomass-t-1)
end

;; PATCH PROCEDURE
to-report biomass-reserve-growth [ green-biomass-t green-biomass-init-t reserve-biomass-t ] ;reserve.max p.growth-rate]
  ;; calculate increase reserve-biomass value
  ;report  (1 - m.r) * reserve-biomass-t + w * ( gr1 * (green-biom-before-grazing - green-biomass-t ) + green-biomass-t ) * ( 1 -  reserve-biomass-t / reserve-max ))
  ifelse p-reserve-max != 0 ;reserve.max != 0
  [
    ;report  ( ( 1 - m.r) * reserve-biomass-t) + p.growth-rate * green-biomass-t * ( 1 -  reserve-biomass-t / reserve.max )
    report ( 1 - m.r ) * reserve-biomass-t + p-growth-rate * ( gr1 * ( green-biomass-init-t - green-biomass-t ) + green-biomass-t ) *
      ( 1 - reserve-biomass-t / p-reserve-max ) ;+ (gr2 * reserve-biomass-t - reserve-biomass-edible-t ) )
  ]
  [
    report 0
  ]
end

to biomass-growth-seasonal
  ;; biomass grows in every season according to seasonal rainfall value
  ask patches
  [
    ; OLD VERSION: reserve biomass growth carried out at beginning of the year, before green biomass
    ; (should not make a big difference, but we want to keep it consistent with other models --> TODO)
    set reserve-biomass round ( biomass-reserve-growth green-biomass green-biomass-before-grazing reserve-biomass ) ;p-reserve-max p-growth-rate)

    ; green biomass will grow depeding on rainfall, which can be either global (homogeneous rain) or local (heterogeneous rain)
    set green-biomass round ( biomass-green-growth green-biomass local-rainfall reserve-biomass )
    set green-biomass-before-grazing green-biomass
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; LIVESTOCK PROCEDURES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;; herd function
to herds-reproduction
  set indiv-reproduction-rate calc-herd-reproduction-rate herd-size-total ; get reproduction rate
  ifelse ( int_herd_size? )
  [
    set herd-size-total herd-size-check round (herd-size-total * (1 + indiv-reproduction-rate )) ; reproduction
  ] [
    set herd-size-total herd-size-check (herd-size-total * (1 + indiv-reproduction-rate )) ; reproduction
  ]
end

to-report calc-herd-reproduction-rate [ current-herd-size ]
  ;; function reports a reproduction rate depending on the current total herd size of the household
  ;; important: herd-size-total is stored per agent, which represents several hh - therefore we need to divide current-herd-size by hh-per-agent

  ; define minimum and mean herd size as reference, reproduction rate r should be at (reproduction-rate - reproduction-rate-range) for Lmin and at reproduction-rate for Lmean
  let Lmin 1
  let Lmean 2 * max-milk-herd-hh

  ; calculate the effective r for the household based on regression y = ax + b
  let a reproduction-rate-range / ( Lmean - Lmin )
  let b reproduction-rate - a * Lmean
  let r-effective a * ( current-herd-size / hh-per-agent ) + b + random-normal 0 0.005

  ; check that r stays within the given range
  if ( r-effective < reproduction-rate - reproduction-rate-range )
  [
    set r-effective reproduction-rate - reproduction-rate-range
  ]
  if ( r-effective > reproduction-rate + reproduction-rate-range )
  [
    set r-effective reproduction-rate + reproduction-rate-range
  ]
  report r-effective
end

;; HERD PROCEDURE
to herds-graze-destock
  ;1. herds have demand = herd size * intake
  let intake yearly-intake / ticks-per-year
  let demand herd-size * intake

  ;2. herds eat as much as possible
  let forage-available max (list ([green-biomass] of patch-here - min-green-biomass) 0)
  ifelse demand <= forage-available
  [
    ;case 1: enough forage
    ask patch-here
    [
      set green-biomass green-biomass - demand
    ]
    set demand 0
  ]
  [
    ;case 2: not enough forage
    set demand demand - forage-available
    ask patch-here
    [
      set green-biomass green-biomass - forage-available
    ]
  ]

  ;3. destocking if demand left
  if demand > 0
  [
    set grazing-destock-counter grazing-destock-counter + demand / intake
    ifelse ( int_herd_size? )
    [
      set herd-size max (list (herd-size -  ceiling ( demand / intake ) ) 0)
    ] [
      set herd-size max (list (herd-size - demand / intake) 0)
    ]
  ]
end

;; HERD PROCEDURE
to herds-move
  ifelse herd-size > 0 [
    ; identify the possible target pastures: accessible pastures of home-settlement (for this season and herd type) with enough green biomass
    let target-pastures nobody
    ifelse foora?
    [
      ifelse rainy-season?
      [
        set target-pastures [ accessible-patches-rainy-foora with [green-biomass > min-green-biomass] ] of home-settlement
      ]
      [
        set target-pastures [ accessible-patches-dry-foora with [green-biomass > min-green-biomass]] of home-settlement
      ]
    ]
    [
      ifelse rainy-season?
      [
        set target-pastures [accessible-patches-rainy-milk with [green-biomass > min-green-biomass]] of home-settlement
      ]
      [
        set target-pastures [accessible-patches-dry-milk with [green-biomass > min-green-biomass]] of home-settlement
      ]
    ]

    ; move to target pasture with maximal green biomass if one exists, else move home
    ifelse any? target-pastures
    [
      move-to max-one-of target-pastures [green-biomass]
    ]
    [
      move-to home-settlement
    ]
  ]
  [
   ; herds with zero herd size are displayed at home
  move-to home-settlement
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; SETTLEMENT PROCEDURES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to settlements-set-accessible-patches
  ;; set up potentially accessible pastures
  ask settlements
  [
    set accessible-patches-rainy-foora patches with [ land-use = "foora" or land-use = "warra"] ; during rainy season foora herds move to foora patches or warra patches
    set accessible-patches-dry-foora patches with [ land-use = "warra"]
    set accessible-patches-rainy-milk patches with [ land-use = "warra"]
    set accessible-patches-dry-milk patches with [ land-use = "enclosure" or land-use = "warra"] ; during dry season milk herds may move to enclosures
  ]

  ;; maybe reduce accessible pastures if moving radius?=true
  if moving-radius?
  [
    foreach [ who ] of settlements
    [ [?1] ->
      let this-settlement turtle ?1
      let p1 patch [ pxcor ] of this-settlement [ pycor ] of this-settlement

      ;; calculate accessibility of patches and maybe reduce accessible pastures wrt season and herd type
      distance-patches p1 radius-dry-foora
      ask this-settlement [set accessible-patches-dry-foora accessible-patches-dry-foora with [dist >= 0]]
      distance-patches p1 radius-dry-milk
      ask this-settlement [set accessible-patches-dry-milk accessible-patches-dry-milk with [dist >= 0]]
      distance-patches p1 radius-rainy-foora
      ask this-settlement [set accessible-patches-rainy-foora accessible-patches-rainy-foora with [dist >= 0]]
      distance-patches p1 radius-rainy-milk
      ask this-settlement [set accessible-patches-rainy-milk accessible-patches-rainy-milk with [dist >= 0]]
    ]
  ]
end

to settlements-set-possible-cultivated-patches
  ; determines the possible cultivated patches for each settlement
  ask settlements [
   set possible-cultivated-patches nobody

   if ( cultivated-soil-types != "none" )
   [
     ; determine possible cultivated patches ignoring the soil type
     set possible-cultivated-patches ifelse-value ( cultivation-on-enclosures? ) ; check if cultivation on enclosures is allowed
     [ patches with [ land-use != "barren" and land-use != "foora" and soil-type != "none" and distance myself < cropland-dist-max and distance myself >= cropland-dist-min ] ]
     [ patches with [ land-use != "enclosure" and land-use != "barren" and land-use != "foora" and soil-type != "none" and distance myself < cropland-dist-max and distance myself >= cropland-dist-min ] ]

     ; if cultivation is only allowed on one soil type reduce the possible cultivated patches accordingly
     if ( cultivated-soil-types != "all" )
     [
       ifelse ( same-max-cult-area-soiltypes? )
       [
        ; if same-max-cult-area-soiltypes? is on, calculate first the number of possible cultivated patches which shall be the same for both soil types
         let possible-patches-count min list count possible-cultivated-patches with [ soil-type = "bottomland" ] count possible-cultivated-patches with [ soil-type = "upland" ]
         set possible-cultivated-patches min-n-of possible-patches-count possible-cultivated-patches with [ soil-type = cultivated-soil-types ] [ distance myself ]
       ]
       [
         set possible-cultivated-patches possible-cultivated-patches with [ soil-type = cultivated-soil-types ]
       ]
     ]
   ]
  ]
end

to settlements-increase-households
  ;; number of households increases in each settlement with a certain growth rate
  ask settlements
  [
    let this-settlement self

    ; calculate number of new households (agents) per settlement, based on the population growth rate
    let number-new-agents round ( pop-growth-rate * count herds with [ not foora? and home-settlement = this-settlement ] )

    ;create a number of milk-herd agents
    if (log-messages?) [
      print ( word ( number-new-agents * hh-per-agent ) " new household[s] created in year " ( floor ( ticks / 4 ) + 1 ) " for settlement " this-settlement )
    ]
    if ( number-new-agents > 0 )
    [
     hatch-herds number-new-agents
     [
       set foora? false
       ; setup initial values
       set cultivated-patches no-patches
       set cultivated-patches-and-areas []
       set years-below-consumption-needed 0
       set land 0
       with-local-randomness [
         ; set herd-size-total randomly between herdsize-init-max and min
         set herd-size-total herd-size-check ( ( herdsize-init-min + random ( herdsize-init-max + 1 - herdsize-init-min ) ) * hh-per-agent)
       ]
       set home-settlement this-settlement
       set shape "cow"

       ; to associate milk and foora herds to each other
       let this-milk-herd self
       let this-foora-herd nobody

       ; create foora-herds ; inherits all variables from parent
       hatch-herds 1
       [
         set foora? true
         set associated-herd this-milk-herd
         set this-foora-herd self
       ]
       set associated-herd this-foora-herd
      ]
    ]
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; HOUSEHOLD PROCEDURES ;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
to households-increase-cultivated-area
  ; calculate the increase in cultivated area for each household in the current year
  ; NEW LOGIC: households increase cultivated area irrespective of their current herd size,
  ; but only if they couldn't satisfy their consumption needs in the previous year

  ; determine the preliminary change in cultivated area:
  ; this does not depend on herd size any more! (no relationship found in IBLI data between herd size and land holding)
  ; cultivated-area-delta-per-hh and cultivated-area-max-per-hh are global parameters defined in the interface
  let delta-cultivated-area cultivated-area-delta-per-hh * hh-per-agent

  ; calculate maximal cultivated area depending on herd-size-total and cultivated-area-max-per-hh
  let cultivated-area-max  cultivated-area-max-per-hh * hh-per-agent

  if ( years-below-consumption-needed > 0) [
    ; household did not consume enough in the previous year(s), so it will increase its cultivated area

    ; calculate the new land and real delta-cultivated-area (may differ from before as land has to be between 0 ha and cultivated-area-max)
    let land-new max ( list min ( list (land + delta-cultivated-area) cultivated-area-max ) 0 )
    set delta-cultivated-area ( land-new - land )
    set land land-new

    ;; now we need to spatially assign the cultivated area, if delta-cultivated-area != 0
    ;; two cases: delta-cultivated-area > 0, delta-cultivated-area < 0
    ;; NEW LOGIC: only consider the case of cultivated area increasing!
    if ( delta-cultivated-area > 0 )
    [
      ; update cultivation-increase-counter for monitoring
      ifelse ( herd-size-total < min-herd-size-rich-increase-cultivation * hh-per-agent ) [ set poor-hh-increase-cultivation poor-hh-increase-cultivation + 1 ]
      [ set rich-hh-increase-cultivation rich-hh-increase-cultivation + 1 ]

      ; go through different patches and try to expand cultivated area on them as long as there is still delta-cultivated-area left
      let loopcount 0
      while [ delta-cultivated-area > 0 ]
      [
        set loopcount loopcount + 1
        ; if looked through 20 patches break
        ifelse ( loopcount > 20 )
        [
          if ( log-messages? ) [ print ( word "while loop > 20") ]
          ; agent cannot increase land by the remaining delta-cultivated-area
          set land land - delta-cultivated-area
          set delta-cultivated-area 0

        ]
        [
          ; first we look through all currently cultivated patches and try to expand cultivated area on them
          ifelse ( any? cultivated-patches with [ patch-cultivated-area < 100 ] )
          [
            ; there is still land available to cultivate on previously cultivated patches, we select one with minimal distance to the home-settlement
            if (log-messages?) [ type ( word who ": still land available to cultivate on previously cultivated patches, delta = " delta-cultivated-area " / " ) ]
            let hs home-settlement
            let selected-patch-for-cultivation min-one-of cultivated-patches with [ patch-cultivated-area < 100 ] [ distance hs ]
            ask selected-patch-for-cultivation
            [
              ; determine how much we can expand cultivation on the selected patch (save as delta-cultivated-area-local)
              let available-patch-cultivated-area 100 - patch-cultivated-area
              let delta-cultivated-area-local min ( list delta-cultivated-area available-patch-cultivated-area )

              if (log-messages?) [ type ( word self " " patch-cultivated-area " current / " ) ]

              ; update patch-cultivated area
              set patch-cultivated-area patch-cultivated-area + delta-cultivated-area-local
              if (log-messages?) [ print ( word patch-cultivated-area " new / delta = " delta-cultivated-area ) ]

              ask myself [
                ; update my list of cultivated-patches-and-areas
                let selected-patch-position position selected-patch-for-cultivation map first cultivated-patches-and-areas
                let updated-patch-entry ( list selected-patch-for-cultivation ( last item selected-patch-position cultivated-patches-and-areas + delta-cultivated-area-local ) )
                set cultivated-patches-and-areas replace-item selected-patch-position cultivated-patches-and-areas updated-patch-entry

                ; update delta-cultivated-area
                set delta-cultivated-area delta-cultivated-area - delta-cultivated-area-local
              ]
            ]
          ]
          [
            ; new patch for cultivation needs to be selected
            if (log-messages?) [ type ( word who ": new patch selected, delta = " delta-cultivated-area " / " ) ]
            let hs home-settlement
            let selected-patch-for-cultivation min-one-of [ possible-cultivated-patches with [ patch-cultivated-area < 100 ] ] of home-settlement [ distance hs ]
            ifelse ( selected-patch-for-cultivation != nobody )
            [
              ask selected-patch-for-cultivation
              [
                ; determine how much we can expand cultivation on the selected patch (save as delta-cultivated-area-local)
                let available-patch-cultivated-area 100 - patch-cultivated-area
                let delta-cultivated-area-local min ( list delta-cultivated-area available-patch-cultivated-area )

                ; update patch-cultivated area
                if (log-messages?) [ type ( word self " " patch-cultivated-area " current / " ) ]
                set patch-cultivated-area patch-cultivated-area + delta-cultivated-area-local

                ;; if patch was not yet cropland, change land use classification
                if ( land-use != "cropland" )
                [
                  set land-use-old land-use ; save old land use to remember if patch turns to pasture again
                  set land-use "cropland"
                ]

                ask myself [
                  ; update my list of cultivated-patches-and-areas
                  set cultivated-patches ( patch-set cultivated-patches selected-patch-for-cultivation )
                  set cultivated-patches-and-areas lput ( list selected-patch-for-cultivation delta-cultivated-area-local ) cultivated-patches-and-areas

                  ;update delta-cultivated-area
                  set delta-cultivated-area delta-cultivated-area - delta-cultivated-area-local
                ]
              ]
              if (log-messages?) [ print ( word [ patch-cultivated-area ] of selected-patch-for-cultivation " new / delta = " delta-cultivated-area ) ]
            ]
            [
              ;there is no patch anymore on which we can expand cultivation -> exit the while loop
              set loopcount  1000
              if ( log-messages? ) [ print ( word "hh " who ": no empty patch available anymore for cultivation! delta = " delta-cultivated-area ) ]
            ]
          ]

        ]
      ]
    ]
  ]
end

to households-consumption
  ;; function to calculate annual consumption of households and determine herd offtake or possible death
  ;; consumption is a function of: milk production, yields from crop farming, and livestock offtake
  ;; household-crop-harvest     ; amount of harvest from crops - calculated for each season (but will most likely be succesful only in rainy seasons)
  ;; household-milk-production  ; production of milk, proportional to milk herd size of the household
  ;; household-herd-offtake     ; calculated as consumption deficit that is not covered by milk and crop harvest (subsumes sold and slaughtered animals)

  ;clear-output

  ;; calculate milk production of the household
  if ( log-messages? ) [ output-print ( word "milk herd size: " (herd-size-milk-check herd-size-total) " / total herd size: " herd-size-total ) ]
  set household-milk-production calc-milk-production (herd-size-milk-check herd-size-total)
  ; HH MILK CONSUMPTION
  set household-milk-consumption household-milk-production * milk_calories_per_kg

  if ( log-messages? ) [ output-print ( word "milk prod.: " household-milk-production " l / milk calories: " household-milk-consumption  ) ]

  ;; calculate crop harvest of the household
  let current-harvest 0
  ;; for every cultivated patch the current harvest is a product of a crop cultivation function depending on
  ;; the rain of the current season and the cultivated area per hh
  ;; total harvest is the sum of these
  foreach cultivated-patches-and-areas [
    [ x ] ->
    let current-local-rain (sublist ([local-rainfall-list] of first x) ticks ( ticks + 4 ))
    let current-local-soiltype n-values 4 [[ soil-type ] of first x ]
    set current-harvest current-harvest + sum ( map calc-crop-harvest current-local-rain current-local-soiltype ) *  ( last x ) *  ( [ p-crop-max ] of first x )
  ]
  set household-crop-harvest current-harvest
  ;; HH CROP CONSUMPTION
  set household-crop-consumption household-crop-harvest * maize_calories_per_kg
  if ( log-messages? ) [ output-print ( word "crop area: " land " ha" ) ]
  if ( log-messages? ) [ output-print ( word "crop harvest: " household-crop-harvest " kg / crop calories: " household-crop-consumption) ]

  ;; calculate the needed herd offtake, if there is a consumption deficit, adjust herd sizes, possibly die
  ;; calculate amount of kcal not yet covered by milk and crops
  let consumption-needed-remainder max ( list ( consumption-needed - household-crop-consumption - household-milk-consumption ) 0 ) ; in kcal!
  if ( log-messages? ) [ output-print ( word  "missing percentage: " ( consumption-needed-remainder / consumption-needed ) ) ]
  set household-herd-offtake calc-herd-offtake herd-size-total consumption-needed-remainder
  if ( log-messages? ) [ output-print ( word "herd offtake: " household-herd-offtake ) ]

  ;; update herd-offtake-counter
  set herd-offtake-counter herd-offtake-counter + household-herd-offtake * hh-per-agent

  ;; calculate if herd offtake that is possible without dropping below min-milk-herd-hh covers the consumption-needed-remainder
  let household-maximum-offtake max list ( herd-size-total - min-milk-herd-hh * hh-per-agent ) 0
  if ( log-messages? ) [ output-print ( word "max possible offtake: " household-maximum-offtake ) ]
  ifelse ( household-herd-offtake <= household-maximum-offtake )
  [
    ;; offtake needed is smaller than maximum offtake, hh can cover his consumption needed
    ;; HH HERD CONSUMPTION
    ifelse ( int_herd_size? )
    [
      ;; if we want integer herd sizes, we need to calculate the real herd consumption inversely
      set household-herd-consumption ( household-herd-offtake * cattle_price_tlu * maize_calories_per_kg ) / maize_price_kg
    ] [
      set household-herd-consumption consumption-needed-remainder ; in kcal???
    ]
    set years-below-consumption-needed 0
    set household-food-security-years-below-one-current 0
    ;; adjust herd size
    set herd-size-total herd-size-check ( herd-size-total - household-herd-offtake )
    ;; update herd-size based on the adjusted herd-size-total
    herd-size-individual-update
    ;; update food security
    set household-food-security ( household-milk-consumption + household-crop-consumption + household-herd-consumption) / consumption-needed
    if ( log-messages? ) [ output-print ( word "herd offtake <= maximum offtake / new total herdsize: " herd-size-total ) ]
  ]
  [
    ;; offtake needed is larger than maximum offtake, household remains below consumption needed
    ;; calculate which share of consumption-needed-remainder can be covered
    let herd-offtake-share household-maximum-offtake / household-herd-offtake
    set household-herd-offtake household-maximum-offtake
    ;; HH HERD CONSUMPTION
    set household-herd-consumption herd-offtake-share * consumption-needed-remainder ; in kcal???
    ;; years-below-consumption increases
    set years-below-consumption-needed years-below-consumption-needed + 1
    set household-food-security-years-below-one-total household-food-security-years-below-one-total + 1
    set household-food-security-years-below-one-current household-food-security-years-below-one-current + 1
    if ( household-food-security-years-below-one-current > household-food-security-years-below-one-longest )
    [
      set household-food-security-years-below-one-longest household-food-security-years-below-one-current
    ]
    ;; adjust herd size or die
    ifelse ( years-below-consumption-needed > 3 and hh-below-consumption-need-die? )
    [
      ;; if death is turned on, after 4 years below consumption needed household dies
      if ( log-messages? )
      [
        print ( word "hh " who " died because it could not cover its consumption need! herd size = " ( herd-size-total / hh-per-agent )
          " consumption-needed-remainder = " consumption-needed-remainder " offtake = " household-herd-offtake )
      ]
      ;; reset cultivated area before hh dies
      foreach cultivated-patches-and-areas
      [
        [ x ] -> let cult-area-on-patch item 1 x
        ask item 0 x
        [
          set patch-cultivated-area max ( list ( patch-cultivated-area - cult-area-on-patch ) 0 ) ; just make sure 0 is the minimum
          if patch-cultivated-area = 0
          [
            set land-use land-use-old
          ]
        ]
      ]
      ;; milk herd and foora herd die
      ask associated-herd
      [
        die
      ]
      die
    ]
    [
      ;; herd size of households below consumption need which don't die is set to min-milk-hh
      if ( log-messages? )
      [
        print ( word "hh " who " below consumption needed since " years-below-consumption-needed " years, herd size = " ( herd-size-total / hh-per-agent )
          " consumption-needed-remainder = " consumption-needed-remainder " offtake = " household-herd-offtake )
      ]
      ;; we need to account for the case that herd-size-total may be 0 (or < min-milk-herd-hh) - i.e. the household has already lost its herd
      ifelse ( herd-size-total < min-milk-herd-hh * hh-per-agent )
      [
        ;; herd-size-total is lower than min milk herd size or 0, so it cannot consume anything
        set herd-size-total herd-size-total ; leave herd size as is, will be updated later with herd-size-update
      ]
      [
        ;; herd-size-total is larger than min milk herd size, so hh consumes all livestock above that
        set herd-size-total min-milk-herd-hh * hh-per-agent
      ]

      ;; update herd-size based on the adjusted herd-size-total
      herd-size-individual-update
      ;; update food security
      set household-food-security ( household-milk-consumption + household-crop-consumption + household-herd-consumption) / consumption-needed
      if ( log-messages? ) [ output-print ( word "herd offtake > maximum offtake / new total herdsize: " herd-size-total ) ]
    ]
  ]
end

to-report calc-milk-production [ _milk_herd_size ]
  ;; calculate the milk production in liters, given the milk herd size of the household
  ; total milk production [l/year]
  report _milk_herd_size * 365 * milk-production-per-day
end

to-report calc-crop-harvest [ _rain _soil_type ]
  ;; calculate crop harvest using a Mitscherlich-Baule crop production function
  ;; based on Janssen et al. 2010
  ;: t_max_ * (1 - exp(b * (rain - rain_min_)))
  ;: t_max_ maximum production adjustment (wet years)
  ;: b curve steepness
  ;; rain_min_ minimum rainfall level required for production
  ;; rain_mean_ rainfall at which prod = 1
  let _harvest 0
  ifelse ( crop-prod-diff-by-soiltypes? and _soil_type = "upland" )
  [
    ;; cropp production is differentiated by soil types, so production on uplands is lower
    let rain_min_ 400 ; minimum rainfall level required for production
    let rain_mean_ 550 ; rainfall level at which crop production should be 1.0
    let t_max_ 1.35
    ; b = - (log((tau_max - 1) / tau_max))/(rain_min - rain_mean)
    let b (- ln ( (t_max_ - 1) / t_max_ ) / ( - rain_min_ - rain_mean_ ))
    ; total crop yield [kg/year]
    set _harvest max ( list 0 ( t_max_ * ( 1 - exp ( b * ( _rain - rain_min_ ) ) ) * crop_yield_mean ) )
  ] [
    ;; crop production on bottomlands (or standard case when soil types are not distinguished)
    let rain_min_ 300 ; minimum rainfall level required for production
    let rain_mean_ 450 ; rainfall level at which crop production should be 1.0
    let t_max_ 1.5
    ; b = - (log((tau_max - 1) / tau_max))/(rain_min - rain_mean)
    let b (- ln ( (t_max_ - 1) / t_max_ ) / ( - rain_min_ - rain_mean_ ))
    ; total crop yield [kg/year]
    set _harvest max ( list 0 ( t_max_ * ( 1 - exp ( b * ( _rain - rain_min_ ) ) ) * crop_yield_mean ) )
  ]
  report _harvest
end

to-report calc-herd-offtake [ _herd-size-total _consumption-needed-remainder ]
  ;; calculate the necessary herd offtake to fulfill consumption needs,
  ;; based on the remainder of consumption needed that cannot be covered by milk and crops
  let _offtake 0
  ;; calculate amount of maize needed to cover that remainder
  let maize_purchase_kg _consumption-needed-remainder / maize_calories_per_kg
  if ( log-messages? ) [ output-print ( word "maize purchase: " maize_purchase_kg  ) ]
  ; calculate herd offtake by estimating how many cattle the household would need to sell in order to purchase the required amount of maize
  ifelse ( int_herd_size? )
  [
    ; if no herd but still consumption needed, the offtake is set 1 to ensure that hh is treated as "below consumption needed" in the following
    set _offtake ifelse-value ( _herd-size-total = 0 and _consumption-needed-remainder > 0) [ 1 ]
      ; no negative herd offtake
      [ max ( list ( ceiling ( ( maize_purchase_kg * maize_price_kg ) / cattle_price_tlu ) ) 0 ) ]
  ] [
    ; if no herd but still consumption needed, the offtake is set 1 to ensure that hh is treated as "below consumption needed" in the following
    set _offtake ifelse-value ( _herd-size-total = 0 and _consumption-needed-remainder > 0) [ 1 ]
      ; no negative herd offtake
      [ max ( list ( ( maize_purchase_kg * maize_price_kg ) / cattle_price_tlu ) 0 ) ]
  ]
  report _offtake
end

to households-regain-herd
  ;; with a certain prob. cultivating households without herd may return to pastoralism
  ;; ask herds with [not foora? and herd-size-total = 0 and land > 0]
  ;; ask herds with [not foora? and herd-size-total = 0 and household-food-security < 1 ]
  set hh_regained_herd 0
  ask herds with [ not foora? and herd-size-total = 0 ]
  [
    if random-float 1 <= pastoralism-return-prob
    [
      set herd-size-total min-milk-herd-hh * hh-per-agent
      set hh_regained_herd hh_regained_herd + 1
    ]
  ]
end
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;









@#$#@#$#@
GRAPHICS-WINDOW
175
55
505
709
-1
-1
12.9
1
10
1
1
1
0
0
0
1
0
24
0
49
0
0
1
ticks
30.0

SLIDER
5
530
170
563
bottomlands-warra
bottomlands-warra
0
1
0.35
0.01
1
NIL
HORIZONTAL

SLIDER
5
495
170
528
uplands-warra
uplands-warra
0
1
0.6
0.01
1
NIL
HORIZONTAL

BUTTON
5
225
60
258
NIL
setup
NIL
1
T
OBSERVER
NIL
S
NIL
NIL
1

TEXTBOX
5
480
157
498
Soil type
11
0.0
1

BUTTON
115
225
170
258
go
go
T
1
T
OBSERVER
NIL
G
NIL
NIL
1

INPUTBOX
5
285
90
345
rain-mean
670.0
1
0
Number

INPUTBOX
90
285
170
345
rain-sd
220.0
1
0
Number

TEXTBOX
1445
10
1625
41
Vegetation model parameters
13
73.0
1

INPUTBOX
1445
30
1595
90
green-biomass-init
50000.0
1
0
Number

INPUTBOX
1445
90
1595
150
reserve-biomass-init
25000.0
1
0
Number

INPUTBOX
1645
270
1700
330
rue
0.002
1
0
Number

INPUTBOX
1700
270
1750
330
lambda
1.6
1
0
Number

INPUTBOX
1595
270
1645
330
w
0.46
1
0
Number

INPUTBOX
1750
270
1800
330
gr1
0.5
1
0
Number

INPUTBOX
1900
270
1995
330
reserve-max
150000.0
1
0
Number

INPUTBOX
1850
270
1900
330
m.r
0.13
1
0
Number

INPUTBOX
1800
270
1850
330
m.g
0.0
1
0
Number

BUTTON
60
225
115
258
tick
go
NIL
1
T
OBSERVER
NIL
T
NIL
NIL
1

INPUTBOX
5
100
75
160
x-steps
400.0
1
0
Number

INPUTBOX
1345
90
1440
150
yearly-intake
1600.0
1
0
Number

CHOOSER
175
710
420
755
patch-color-view
patch-color-view
"soil-type" "green-biomass" "land-use" "green-biomass-min" "reserve-biomass"
2

INPUTBOX
2000
235
2160
295
cropland-dist-min
0.0
1
0
Number

INPUTBOX
2000
295
2160
355
cropland-dist-max
17.0
1
0
Number

BUTTON
420
710
505
755
update view
view-patches\n
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

INPUTBOX
1445
150
1595
210
upland-growth-rate-factor
0.3
1
0
Number

INPUTBOX
1445
210
1595
270
upland-reserve-max-factor
0.6
1
0
Number

MONITOR
275
10
365
55
NIL
rainy-season?
17
1
11

MONITOR
225
10
275
55
NIL
season
17
1
11

SLIDER
5
600
170
633
bottomlands-foora
bottomlands-foora
0
1
0.15
0.01
1
NIL
HORIZONTAL

SLIDER
5
565
170
598
uplands-foora
uplands-foora
0
1
0.83
0.01
1
NIL
HORIZONTAL

INPUTBOX
1205
30
1305
90
reproduction-rate
0.58
1
0
Number

INPUTBOX
1110
90
1225
150
max-milk-herd-hh
7.0
1
0
Number

PLOT
1595
30
1995
150
green biomass by pasture type
NIL
kg / km^2
0.0
10.0
0.0
10.0
false
true
"" "ifelse ( ticks > 0 ) \n[ \n  set-plot-x-range 0 ticks\n\n  set-plot-y-range 0 ceiling max ( list green_enclosure green_foora green_warra plot-y-max ) \n] \n[ \n  set-plot-x-range 0 1 \n  set-plot-y-range 0 1\n]"
PENS
"enclosure" 1.0 0 -10899396 true "" "plotxy ticks green_enclosure"
"foora" 1.0 0 -817084 true "" "plotxy ticks green_foora"
"warra" 1.0 0 -7500403 true "" "plotxy ticks green_warra"
"min-green-biomass" 1.0 0 -1264960 false "" "plotxy ticks min-green-biomass"

PLOT
510
285
995
435
Herds and Cultivation
NIL
#hh
0.0
10.0
0.0
1.0
false
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks ]\n[ set-plot-x-range 0 1 ]\nifelse ( any? herds )\n[ set-plot-y-range 0 max ( list ( agents-per-settlement * 3 * hh-per-agent) (count herds with [ foora? ] * hh-per-agent) plot-y-max ) ] ;( count herds with [ foora? ] ) * hh-per-agent ]\n[ set-plot-y-range 0 10 ]"
PENS
"foora" 1.0 0 -2674135 true "" "plotxy ticks hh_foora"
"milk herd" 1.0 0 -13840069 true "" "plotxy ticks hh_warra"
"cultivating" 1.0 0 -4079321 true "" "plotxy ticks hh_cultivating"
"no herds" 1.0 0 -12895429 true "" "plotxy ticks hh_without_herd"
"min herd" 1.0 0 -7500403 true "" "plotxy ticks hh_herd_min"
"fsec < 1" 1.0 0 -11221820 true "" "plotxy ticks hh_fsec_below_one"
"fsec >= 1" 1.0 0 -13345367 true "" "plotxy ticks hh_fsec_above_one"

INPUTBOX
1225
90
1345
150
max-foora-herd-hh
99999.0
1
0
Number

INPUTBOX
75
100
170
160
seed
8218.0
1
0
Number

SWITCH
5
160
170
193
fixed-seed?
fixed-seed?
1
1
-1000

INPUTBOX
1000
90
1110
150
min-milk-herd-hh
2.0
1
0
Number

INPUTBOX
1885
330
1995
390
min-green-biomass
5000.0
1
0
Number

PLOT
1595
150
1995
270
reserve biomass by pasture type
NIL
kg / km^2
0.0
1.0
0.0
0.1
false
true
"" "ifelse ( ticks > 0 ) \n[ \n  set-plot-x-range 0 ticks\n  ;let m1 mean [ reserve-biomass ] of patches with [soil-type != \"none\" and land-use = \"enclosure\"]\n  ;let m2 mean [ reserve-biomass ] of patches with [soil-type != \"none\" and land-use = \"foora\"]\n  ;let m3 ifelse-value (any? patches with [land-use = \"warra\"]) [ mean [ reserve-biomass ] of patches with [soil-type != \"none\" and land-use = \"warra\"]] [0]\n  set-plot-y-range 0 ceiling max ( list reserve_enclosure reserve_foora reserve_warra plot-y-max )\n] \n[ \n  set-plot-x-range 0 1 \n  set-plot-y-range 0 0.1\n]"
PENS
"enclosure" 1.0 0 -10899396 true "" "plotxy ticks reserve_enclosure"
"foora" 1.0 0 -817084 true "" "plotxy ticks reserve_foora"
"warra" 1.0 0 -7500403 true "" "plotxy ticks reserve_warra"

PLOT
1000
490
1215
610
total cropland area
NIL
ha
0.0
1.0
0.0
0.1
false
false
"" "ifelse ( ticks > 0 )\n[ \n  set-plot-x-range 0 ticks \n  set-plot-y-range 0 ceiling max ( list plot-y-max cult_area_sum ( count patches with [ land-use = \"cropland\" ] * 100 ) )\n]\n[ \n  set-plot-x-range 0 1 \n  set-plot-y-range 0 1\n]"
PENS
"cultiv-area" 1.0 0 -2674135 true "" "plotxy ticks ( count patches with [ land-use = \"cropland\" ] * 100 )"
"land" 1.0 0 -7500403 true "" "plotxy ticks cult_area_sum"

SLIDER
5
30
170
63
agents-per-settlement
agents-per-settlement
0
500
120.0
1
1
NIL
HORIZONTAL

SLIDER
5
65
170
98
hh-per-agent
hh-per-agent
0
100
10.0
1
1
NIL
HORIZONTAL

MONITOR
365
10
425
55
hh-initial
agents-per-settlement * 3 * hh-per-agent
0
1
11

PLOT
1215
490
1440
610
herd size distribution
NIL
tlu per hh
0.0
1.0
0.0
1.0
true
false
"" "set-plot-pen-mode 1"
PENS
"consumption" 1.0 0 -1184463 true "" "plot-herd-size-hist2"
"warra" 1.0 0 -10899396 true "" ""
"foora" 1.0 0 -2674135 true "" ""
"total" 1.0 0 -16777216 true "" ""

CHOOSER
1000
310
1160
355
cultivated-soil-types
cultivated-soil-types
"bottomland" "upland" "all" "none"
2

SWITCH
2000
480
2160
513
cultivated-area-constant?
cultivated-area-constant?
1
1
-1000

PLOT
510
10
995
130
Rainfall
NIL
mm/year
0.0
10.0
0.0
10.0
false
true
"" "plot-local-rainfall"
PENS
"rain" 1.0 0 -16777216 true "" ""
"rain-thresh" 1.0 0 -1604481 true "" ""

SWITCH
2000
515
2160
548
crops-enclosures-barriers?
crops-enclosures-barriers?
1
1
-1000

PLOT
1215
610
1440
730
land cultivation distribution
NIL
ha per hh
0.0
10.0
0.0
10.0
true
false
"" ""
PENS
"default" 1.0 0 -16777216 true "" "plot-land-distr"

INPUTBOX
1295
295
1440
355
pastoralism-return-prob
0.02
1
0
Number

INPUTBOX
5
650
90
710
enclosure-size
80.0
1
0
Number

TEXTBOX
10
635
115
655
Enclosures (km^2)
11
0.0
1

INPUTBOX
90
650
170
710
enclosure-distance
2.0
1
0
Number

MONITOR
175
10
225
55
Year
floor ( ticks / 4 ) + 1
17
1
11

SWITCH
175
815
340
848
export-plots?
export-plots?
1
1
-1000

TEXTBOX
5
10
135
28
System parameters
13
73.0
1

TEXTBOX
5
265
135
283
Climatic parameters
13
73.0
1

TEXTBOX
1000
10
1150
28
Livestock parameters
13
73.0
1

INPUTBOX
1000
30
1100
90
herdsize-init-min
5.0
1
0
Number

INPUTBOX
1100
30
1205
90
herdsize-init-max
5.0
1
0
Number

TEXTBOX
1000
290
1130
308
Cultivation 
13
73.0
1

PLOT
1000
150
1160
285
Lorenz Curve
NIL
NIL
0.0
10.0
0.0
10.0
false
false
"" ""
PENS
"foora" 1.0 0 -2674135 true "" "plot-lorenz-foora true"
"warra" 1.0 0 -10899396 true "" "plot-lorenz-warra true"
"pen-2" 1.0 0 -7500403 true "" "plotxy 0 0\nplotxy count herds with [ foora? ] 1"
"total" 1.0 0 -16777216 true "" "plot-lorenz-total true"
"fsec" 1.0 0 -13345367 true "" "plot-lorenz-fsec true"

INPUTBOX
1305
30
1440
90
reproduction-rate-range
0.0
1
0
Number

SWITCH
5
345
170
378
global-rainfall?
global-rainfall?
1
1
-1000

PLOT
2000
110
2160
230
Number of hh
NIL
NIL
0.0
10.0
3000.0
3001.0
false
false
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks ]\n[ set-plot-x-range 0 1 ]\nifelse ( any? herds ) \n[ set-plot-y-range 0 ( max ( list ( count herds with [ foora? ] * hh-per-agent ) plot-y-max ) ) ] ;( min ( list ( count herds with [ foora? ] * hh-per-agent ) plot-y-min ) )\n[ set-plot-y-range 0 agents-per-settlement * 3 * hh-per-agent ] ;agents-per-settlement * nr-settlements * hh-per-agent - 100"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks count herds with [not foora? ] * hh-per-agent"

MONITOR
425
10
505
55
hh-current
count herds with [ foora? ] * hh-per-agent
17
1
11

INPUTBOX
2000
30
2160
105
pop-growth-rate
0.0
1
0
Number

TEXTBOX
2000
10
2150
28
Household demographics
13
73.0
1

INPUTBOX
5
415
170
475
rain-spatial-sd
50.0
1
0
Number

SWITCH
1445
350
1645
383
households-consumption?
households-consumption?
0
1
-1000

TEXTBOX
1445
335
1610
353
Household consumption
13
73.0
1

PLOT
1445
385
1765
520
mean consumption per hh
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  ;set-plot-y-range 0 ceiling max ( list ( milk_consumption_mean + crop_consumption_mean + herd_consumption_mean ) plot-y-max ) \n  ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 3.1 ]"
PENS
"m" 1.0 0 -1664597 true "" "plotxy ticks milk_consumption_mean"
"m+c" 1.0 0 -4079321 true "" "plotxy ticks milk_consumption_mean + crop_consumption_mean"
"m+c+l" 1.0 0 -8053223 true "" "plotxy ticks milk_consumption_mean + crop_consumption_mean + herd_consumption_mean"
"pen-3" 1.0 0 -7500403 false "" "plotxy ticks 1"

SWITCH
175
785
340
818
log-messages?
log-messages?
1
1
-1000

PLOT
510
130
995
285
mean herdsize per hh
NIL
tlu/hh
0.0
10.0
0.0
10.0
false
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks ]\n[ set-plot-x-range 0 1 ]\nifelse ( any? herds )\n[ set-plot-y-range 0 ceiling max ( list (herd_size_foora_mean + herd_size_milk_mean) plot-y-max ) ]\n[ set-plot-y-range 0 10 ]"
PENS
"total-longterm" 1.0 1 -5987164 false "" "if (plot-longterm?)\n[\n  plotxy ticks herd_size_total_mean_longterm\n]"
"milk-longterm" 1.0 1 -5509967 false "" "if (plot-longterm?)\n[\nifelse (herd_size_milk_mean_longterm > herd_size_foora_mean_longterm )\n[\n  set-plot-pen-color lime + 3\n  plotxy ticks herd_size_milk_mean_longterm\n]\n[\n  set-plot-pen-color red + 3\n  plotxy ticks herd_size_foora_mean_longterm\n]\n]\n"
"foora-longterm" 1.0 1 -1069655 false "" "if (plot-longterm?)\n[\nifelse (herd_size_milk_mean_longterm < herd_size_foora_mean_longterm )\n[\n  set-plot-pen-color lime + 3\n  plotxy ticks herd_size_milk_mean_longterm\n]\n[\n  set-plot-pen-color red + 3\n  plotxy ticks herd_size_foora_mean_longterm\n]\n]"
"foora herd" 1.0 0 -2674135 true "" "plotxy ticks herd_size_foora_mean"
"milk herd" 1.0 0 -13840069 true "" "plotxy ticks herd_size_milk_mean"
"total" 1.0 0 -16777216 true "" "plotxy ticks herd_size_foora_mean + herd_size_milk_mean"

SWITCH
340
785
505
818
plot-longterm?
plot-longterm?
1
1
-1000

BUTTON
90
755
170
788
record-start
;carefully \n;[ vid:start-recorder print \"recorder started\"] \n;[ user-message error-message ]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
90
785
170
818
record-save
;if vid:recorder-status = \"inactive\" [\n;  user-message \"The recorder is inactive. There is nothing to save.\"\n;  stop\n;]\n;let path user-new-file\n;if not is-string? path [ stop ]  ; stop if user canceled\n; export the movie\n;carefully [\n;  vid:save-recording path\n;  user-message (word \"Exported movie to \" path \".\")\n;] [\n;  user-message error-message\n;]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
90
815
170
848
record-reset
let message (word\n  \"If you reset the recorder, the current recording will be lost.\"\n  \"Are you sure you want to reset the recorder?\")\n;if vid:recorder-status = \"inactive\" or user-yes-or-no? message [\n;  vid:reset-recorder\n;]
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
340
815
505
848
view-clock?
view-clock?
1
1
-1000

SWITCH
1215
415
1440
448
same-max-cult-area-soiltypes?
same-max-cult-area-soiltypes?
1
1
-1000

MONITOR
1765
385
1925
430
milk-production
milk_production_mean_longterm
2
1
11

MONITOR
1765
430
1925
475
crop-harvest
crop_harvest_mean_longterm
2
1
11

MONITOR
1765
475
1925
520
herd-offtake
herd_offtake_mean_longterm
2
1
11

INPUTBOX
1160
355
1315
415
cultivated-area-max-per-hh
8.0
1
0
Number

PLOT
1000
610
1215
730
# cultivated-patches
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "ifelse ( ticks > 0 )\n[ \n  set-plot-x-range 0 ticks \n  set-plot-y-range 0 ceiling max ( list mean [ count cultivated-patches ] of herds with [ not foora? ] 1 )\n]\n[ \n  set-plot-x-range 0 1 \n  set-plot-y-range 0 1\n]"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks ifelse-value ( any? herds with [ not foora? ] )\n[ mean [ count cultivated-patches ] of herds with [ not foora? ] ]\n[ 0 ]"

PLOT
1765
640
1925
775
years of hh below cons. needed
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "let max-years-below-consumption-needed ifelse-value (ticks = 0) [0] [ max [ years-below-consumption-needed ] of herds with [ not foora? ] ]\nifelse ( max-years-below-consumption-needed > 0 )\n[ set-plot-x-range 0 max (list ( max-years-below-consumption-needed + 1) 6 ) ]\n[ set-plot-x-range 0 6 ]\n"
PENS
"default" 1.0 1 -16777216 true "" "histogram [ years-below-consumption-needed ] of herds with [ not foora? ]"

SWITCH
1215
450
1440
483
cultivated-area-decrease?
cultivated-area-decrease?
1
1
-1000

SWITCH
1645
350
1885
383
hh-below-consumption-need-die?
hh-below-consumption-need-die?
1
1
-1000

SWITCH
340
755
505
788
view-crop-shares?
view-crop-shares?
0
1
-1000

PLOT
1445
640
1765
775
years fsec < 1
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 max ( list 1.1 plot-y-max max [ years-below-consumption-needed ] of herds with [ not foora? ] ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1.1 ]"
PENS
"mean" 1.0 0 -16777216 true "" "plotxy ticks food_security_years_below_one_mean"
"max" 1.0 0 -7500403 true "" "plotxy ticks food_security_years_below_one_max"
"longest" 1.0 0 -2674135 true "" "plotxy ticks food_security_years_below_one_longest"
"min" 1.0 0 -955883 true "" "plotxy ticks food_security_years_below_one_min"

SWITCH
1000
415
1215
448
cultivation-on-enclosures?
cultivation-on-enclosures?
1
1
-1000

INPUTBOX
1160
295
1295
355
cultivation-start-year
0.0
1
0
Number

MONITOR
1365
150
1440
195
gini-total
gini_coef_total
3
1
11

MONITOR
1365
195
1440
240
gini-milk
gini_coef_warra
3
1
11

MONITOR
1365
240
1440
285
gini-foora
gini_coef_foora
3
1
11

SWITCH
5
380
170
413
weighted-sd?
weighted-sd?
0
1
-1000

INPUTBOX
2000
355
2160
415
max-herd-size-poor-increase-cultivation
3.0
1
0
Number

INPUTBOX
2000
415
2160
475
min-herd-size-rich-increase-cultivation
8.0
1
0
Number

INPUTBOX
1445
270
1595
330
upland-crop-max-factor
1.0
1
0
Number

PLOT
1605
520
1765
640
Crop yield [kg]
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 max ( list ( ceiling crop_harvest_mean / hh-per-agent ) plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1 ]"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks crop_harvest_mean / hh-per-agent"

PLOT
1445
520
1605
640
Milk production [l]
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 ceiling max ( list ( milk_production_mean / hh-per-agent ) plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1 ]"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks milk_production_mean / hh-per-agent"

INPUTBOX
1000
355
1160
415
cultivated-area-delta-per-hh
0.5
1
0
Number

OUTPUT
1000
735
1440
900
10

PLOT
1765
520
1925
640
Herd offtake [TLU]
NIL
NIL
0.0
10.0
0.0
10.0
true
false
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 max ( list ( ceiling herd_offtake_mean / hh-per-agent ) plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1 ]"
PENS
"default" 1.0 0 -16777216 true "" "plotxy ticks herd_offtake_mean / hh-per-agent"

INPUTBOX
1315
355
1440
415
crop_yield_mean
750.0
1
0
Number

PLOT
1160
150
1365
285
Herd size by land holding
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 ceiling max ( list herd_size_total_no_land_mean herd_size_total_with_land_mean plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1 ]"
PENS
"land=0" 1.0 0 -16777216 true "" "plotxy ticks herd_size_total_no_land_mean"
"land>0" 1.0 0 -2674135 true "" "plotxy ticks herd_size_total_with_land_mean"

PLOT
510
435
995
570
HH Food Security
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 max ( list ( precision food_security_max 1 ) plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1.1 ]"
PENS
"pen-3" 1.0 0 -9276814 false "" ";plotxy ticks 1"
"pen-4" 1.0 0 -16777216 false "" ";plotxy ticks food_security_min"
"pen-5" 1.0 0 -16777216 false "" ";plotxy ticks food_security_max"
"mean" 1.0 0 -2674135 true "" "plotxy ticks food_security_mean"
"no_land" 1.0 0 -10022847 true "" "plotxy ticks food_security_no_land_mean"
"with_land" 1.0 0 -3508570 true "" "plotxy ticks food_security_with_land_mean"
"no_herd" 1.0 0 -14730904 true "" "plotxy ticks food_security_no_herd_mean"
"with_herd" 1.0 0 -8020277 true "" "plotxy ticks food_security_with_herd_mean"

SWITCH
5
190
170
223
nlrx?
nlrx?
1
1
-1000

SWITCH
1000
450
1215
483
crop-prod-diff-by-soiltypes?
crop-prod-diff-by-soiltypes?
0
1
-1000

PLOT
510
570
995
710
HH Food Security 2
NIL
NIL
0.0
10.0
0.0
10.0
true
true
"" "ifelse ( ticks > 0 )\n[ set-plot-x-range 0 ticks \n  set-plot-y-range 0 max ( list ( precision food_security_mean 1 ) plot-y-max ) ]\n[ set-plot-x-range 0 1 \n  set-plot-y-range 0 1.1 ]"
PENS
"default" 1.0 0 -7500403 false "" "plotxy ticks 1"
"only_herd" 1.0 0 -6459832 true "" ";plotxy ticks food_security_only_herd"
"only_land" 1.0 0 -5825686 true "" ";plotxy ticks food_security_only_land"
"both" 1.0 0 -2674135 true "" ";plotxy ticks food_security_both_herd_land"
"fsec < 1" 1.0 0 -11221820 true "" "plotxy ticks food_security_fsec_below_one_mean"
"fsec >= 1" 1.0 0 -13345367 true "" "plotxy ticks food_security_fsec_above_one_mean"

MONITOR
510
710
585
755
herds+land
hh-per-agent * count herds with [ not foora? and herd-size-total > 0 and land > 0]
17
1
11

MONITOR
660
710
735
755
land only
hh-per-agent * count herds with [ not foora? and herd-size-total = 0 and land > 0]
17
1
11

MONITOR
585
710
660
755
herds only
hh-per-agent * count herds with [ not foora? and herd-size-total > 0 and land = 0]
17
1
11

MONITOR
735
710
810
755
nothing
hh-per-agent * count herds with [ not foora? and herd-size-total = 0 and land = 0]
17
1
11

MONITOR
510
755
585
800
fsec
food_security_both_herd_land
3
1
11

MONITOR
585
755
660
800
fsec
food_security_only_herd
3
1
11

MONITOR
660
755
735
800
fsec
food_security_only_land
3
1
11

MONITOR
735
755
810
800
fsec
food_security_nothing
3
1
11

MONITOR
510
800
585
845
herd
herd_size_total_both_herd_land
3
1
11

MONITOR
585
800
661
845
herd
herd_size_total_no_land_mean
3
1
11

MONITOR
660
845
735
890
land
cult_area_only_land
3
1
11

MONITOR
510
845
585
890
land
cult_area_both_herd_land
3
1
11

TEXTBOX
695
815
805
833
0                0
13
0.0
1

TEXTBOX
620
860
845
891
0                                   0
13
0.0
1

MONITOR
885
710
995
755
NIL
hh_regained_herd
2
1
11

SWITCH
175
755
340
788
enclosure-outline?
enclosure-outline?
0
1
-1000

MONITOR
1445
780
1515
825
longest
food_security_years_below_one_longest
3
1
11

MONITOR
1515
780
1603
825
hh_years>0
hh_fsec_years_below_one
3
1
11

INPUTBOX
885
760
995
820
param_set
A
1
0
String

MONITOR
2000
594
2162
639
any non-int herd sizes?
any? herds with [ not foora? and herd-size-total - int herd-size-total > 0 ]
17
1
11

SWITCH
2000
555
2162
588
int_herd_size?
int_herd_size?
1
1
-1000

PLOT
1925
640
2160
775
consumption_shares
NIL
NIL
0.0
10.0
0.0
10.0
false
true
"" "plot-consumption-shares"
PENS
"milk_<1" 1.0 1 -11085214 true "" ""
"herd_<1" 1.0 1 -10649926 true "" ""
"crop_<1" 1.0 1 -987046 true "" ""
"milk_>=1" 1.0 1 -14439633 true "" ""
"herd_>=1" 1.0 1 -14070903 true "" ""
"crop_>=1" 1.0 1 -4079321 true "" ""

MONITOR
810
710
877
755
gini-fsec
gini_coef_fsec
3
1
11

MONITOR
810
755
877
800
gini-land
gini_coef_land
3
1
11

MONITOR
835
825
995
870
max. area available for cult.
cult_area_max_avail_ha
17
1
11

BUTTON
90
850
153
883
bla
let fsec_string \"c(\"\nforeach sort herds with [ not foora? ] [\n  x -> set fsec_string ( word fsec_string [ household-food-security ] of x \",\" )\n]\nset fsec_string ( word but-last fsec_string \")\" )\nprint fsec_string\nprint length fsec_string
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

SWITCH
175
845
340
878
save-indiv-hh?
save-indiv-hh?
0
1
-1000

@#$#@#$#@
## WHAT IS IT?

The Land Use Competition in Drylands (LUCID) model is a joint effort of the Helmholtz Centre for Environmental Research - UFZ and the International Livestock Research Institute (ILRI) to use simulation modeling as a tool for supporting land use planning processes, and to address decision-making around competition among alternative land uses in pastoral and agro-pastoral drylands.

LUCID allows for manipulation of particular parameters and relationships in the model for easy development of alternative scenarios.

## HOW IT WORKS

The model captures key elements of the land use dynamics that have been identified from empirical research by the authors, expert opinion, and published research.  These elements include the following:

- A VIRTUAL LANDSCAPE that represents, in a stylized manner, a landscape in a dryland region where there is competition for land uses, particularly between pastures and cropland. The landscape is spatially divided into different land use categories (pastures, cropland).  Underneath, a layer of soil types based on topography (uplands, bottomlands) defines the productivity of the land.

- HOUSEHOLD AND HERD DYNAMICS such as herd relocation between pastures, livestock reproduction and herd growth, and the households’ decisions to adopt and expand, and in some cases abandon, crop cultivation.

- A DYNAMIC VEGETATION MODEL that simulates biomass growth on the pastures which is influenced by precipitation, the productivity of the soil and feeding by the livestock.

By adjusting model rules and parameters, we can simulate different scenarios such as where croplands are allowed to expand or how much pasture area should be reserved for enclosures. In the following pages, we will describe the main elements and processes of the model.

More details on the model elements and processes can be found in the accompanying project report that is available for download at the link provided below.

## CREDITS AND REFERENCES

Dressler, G., Robinson, L., Müller, B. and Hase, N. (2016): _The LUCID Model and Its Role in Supporting Land Use Planning Processes in Southern Ethiopia_, ILRI Project Report, December 2016.

For more details visit http://www.ufz.de/index.php?en=42265 or contact gunnar.dressler@ufz.de or l.robinson@cgiar.org
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

0
false
0
Polygon -7500403 true true 105 15 195 15 225 30 240 45 255 75 255 150 210 150 210 90 195 60 180 45 120 45 105 60 90 90 90 150 45 150 45 75 60 45 75 30
Polygon -7500403 true true 105 285 195 285 225 270 240 255 255 225 255 150 210 150 210 210 195 240 180 255 120 255 105 240 90 210 90 150 45 150 45 225 60 255 75 270

1
false
0
Polygon -7500403 true true 210 15 210 285 150 285 150 90 75 105 75 45

100_percent
false
0
Rectangle -7500403 true true 0 0 300 300

10_percent
false
0
Rectangle -7500403 true true 0 270 315 300

20_percent
false
0
Rectangle -7500403 true true 0 240 300 315

30_percent
false
0
Rectangle -7500403 true true 0 210 315 315

40_percent
false
0
Rectangle -7500403 true true 0 180 300 300

50_percent
false
0
Rectangle -7500403 true true 0 150 300 300

60_percent
false
0
Rectangle -7500403 true true 0 120 300 300

70_percent
false
0
Rectangle -7500403 true true 0 90 300 300

80_percent
false
0
Rectangle -7500403 true true 0 60 300 300

90_percent
false
0
Rectangle -7500403 true true 0 30 300 300

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

block
false
0
Rectangle -7500403 true true 0 75 300 225

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

clock_arrow
true
0
Polygon -7500403 true true 150 31 128 75 143 75 143 150 158 150 158 75 173 75
Circle -16777216 true false 135 135 30

clockface1
false
0
Circle -7500403 true true 0 0 300
Rectangle -16777216 true false 143 0 158 60
Rectangle -16777216 true false 143 241 158 301
Rectangle -16777216 true false 0 142 60 157
Rectangle -16777216 true false 240 142 300 157

clockface2
false
0
Circle -7500403 true true 0 0 300
Line -16777216 false 0 150 45 150
Line -16777216 false 300 150 255 150
Line -16777216 false 150 0 150 45
Line -16777216 false 150 300 150 255
Line -16777216 false 45 255 60 240
Line -16777216 false 240 60 255 45
Line -16777216 false 60 60 45 45
Line -16777216 false 255 255 240 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cropland
false
0
Rectangle -10899396 true false 0 0 300 300
Polygon -6459832 true false 255 300 300 180 300 105 225 300 255 300
Polygon -6459832 true false 195 300 300 30 300 0 285 0 165 300 195 300
Polygon -6459832 true false 165 0 45 300 75 300 195 0 165 0
Polygon -6459832 true false 225 150 120 150
Polygon -6459832 true false 225 0 105 300 135 300 255 0 225 0
Polygon -6459832 true false 45 0 0 120 0 195 75 0 45 0
Polygon -6459832 true false 105 0 0 270 0 300 15 300 135 0 105 0

cropland2
false
0
Rectangle -6459832 true false 0 0 300 300
Polygon -10899396 true false 120 90 195 135 255 75 180 0 120 90
Polygon -10899396 true false 45 0 0 75 60 135 135 60 45 0
Polygon -10899396 true false 0 195 75 240 135 180 60 135 0 195
Polygon -10899396 true false 120 225 195 270 270 180 195 135 135 195
Rectangle -10899396 true false 105 105 105 135
Polygon -10899396 true false 105 105 75 135 135 165 165 135 135 105
Polygon -10899396 true false 105 225 75 285 150 300 165 255
Polygon -10899396 true false 210 270 285 300 285 225 240 210
Polygon -10899396 true false 225 30 270 60 300 30 255 0 240 15
Polygon -10899396 true false 60 255 30 225 0 255 45 285 60 255
Polygon -10899396 true false 225 120 270 150 300 120 270 90 240 105

croplands10
true
0
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 0 30 30 15 90 15 135 30 150 105 90 210 0 255
Polygon -10899396 true false 180 300 255 300 270 255 285 75 240 45 195 180 165 210 120 255

croplands11
true
0
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 270 300 300 285 300 165 255 150 150 300 270 300
Polygon -14835848 true false 30 0 0 30 0 300 60 225 105 15 60 0
Polygon -10899396 true false 120 180 120 150 135 135 150 105 165 60 180 15 195 0 240 0 270 0 285 0 300 30 300 90 285 105 255 90 225 105 210 120 195 165 180 195 150 195

croplands12
true
0
Rectangle -7500403 true true 0 0 0 15
Polygon -1184463 true false 75 255 135 0 30 15 0 285 30 300
Polygon -7500403 true true 255 180 285 120 300 60 285 0 210 0 195 45 105 300 195 285

croplands3
false
0
Rectangle -7500403 true true 0 0 300 300
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 255 285 285 270 285 195 240 180 180 285 255 285
Polygon -14835848 true false 45 15 15 30 15 105 60 120 120 15 45 15
Polygon -6459832 true false 285 180 255 165 270 90 285 60 285 180
Polygon -10899396 true false 15 120 45 135 30 210 15 240 15 120
Polygon -10899396 true false 165 285 225 180 165 150 75 285 165 285
Polygon -10899396 true false 135 15 75 120 135 150 225 15 135 15
Polygon -14835848 true false 240 165 270 60 285 30 285 30 285 15 240 15 165 135 240 165
Polygon -6459832 true false 60 135 30 240 15 270 15 270 15 285 60 285 120 165 60 135

croplands4
false
0
Rectangle -6459832 true false 0 0 300 300
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 255 285 285 270 285 195 240 180 180 285 255 285
Polygon -14835848 true false 45 15 15 30 15 105 60 120 120 15 45 15
Polygon -13840069 true false 285 180 255 165 270 90 285 60 285 180
Polygon -10899396 true false 15 120 45 135 30 210 15 240 15 120
Polygon -10899396 true false 165 285 225 180 165 150 75 285 165 285
Polygon -10899396 true false 135 15 75 120 135 150 225 15 135 15
Polygon -14835848 true false 240 165 270 60 285 30 285 30 285 15 240 15 165 135 240 165
Polygon -13840069 true false 60 135 30 240 15 270 15 270 15 285 60 285 120 165 60 135

croplands6
false
0
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 255 285 285 270 285 195 240 180 180 285 255 285
Polygon -14835848 true false 45 15 15 30 15 285 75 225 120 15 45 15
Polygon -13840069 true false 165 285 225 180 165 150 45 285 165 285
Polygon -13840069 true false 135 15 105 120 135 165 225 15 135 15
Polygon -14835848 true false 285 150 270 60 285 30 285 30 285 15 240 15 180 135 255 165

croplands7
true
0
Rectangle -6459832 true false 0 0 300 300
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 255 285 285 270 285 150 240 135 135 285 255 285
Polygon -14835848 true false 45 15 15 30 15 285 75 225 120 15 45 15
Polygon -14835848 true false 45 285 90 240 105 180 135 120 150 75 165 30 180 15 225 15 255 15 270 15 285 45 285 105 270 120 240 105 195 135 135 240 90 285 45 285 45 285

croplands8
true
0
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 255 285 285 270 285 150 240 135 135 285 255 285
Polygon -14835848 true false 45 15 15 30 15 285 75 225 120 15 45 15
Polygon -10899396 true false 105 195 105 165 120 150 135 120 150 75 165 30 180 15 225 15 255 15 270 15 285 45 285 105 270 120 240 105 210 120 195 135 180 180 165 210 135 210

croplands9
true
0
Rectangle -7500403 true true 0 0 0 15
Polygon -14835848 true false 15 30 45 15 195 15 165 120 120 165 105 225 15 285
Polygon -10899396 true false 90 285 255 285 285 270 285 60 240 15 195 165 165 195 135 255

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

sheep
false
15
Circle -1 true true 203 65 88
Circle -1 true true 70 65 162
Circle -1 true true 150 105 120
Polygon -7500403 true false 218 120 240 165 255 165 278 120
Circle -7500403 true false 214 72 67
Rectangle -1 true true 164 223 179 298
Polygon -1 true true 45 285 30 285 30 240 15 195 45 210
Circle -1 true true 3 83 150
Rectangle -1 true true 65 221 80 296
Polygon -1 true true 195 285 210 285 210 240 240 210 195 210
Polygon -7500403 true false 276 85 285 105 302 99 294 83
Polygon -7500403 true false 219 85 210 105 193 99 201 83

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tile stones
false
0
Polygon -7500403 true true 0 240 45 195 75 180 90 165 90 135 45 120 0 135
Polygon -7500403 true true 300 240 285 210 270 180 270 150 300 135 300 225
Polygon -7500403 true true 225 300 240 270 270 255 285 255 300 285 300 300
Polygon -7500403 true true 0 285 30 300 0 300
Polygon -7500403 true true 225 0 210 15 210 30 255 60 285 45 300 30 300 0
Polygon -7500403 true true 0 30 30 0 0 0
Polygon -7500403 true true 15 30 75 0 180 0 195 30 225 60 210 90 135 60 45 60
Polygon -7500403 true true 0 105 30 105 75 120 105 105 90 75 45 75 0 60
Polygon -7500403 true true 300 60 240 75 255 105 285 120 300 105
Polygon -7500403 true true 120 75 120 105 105 135 105 165 165 150 240 150 255 135 240 105 210 105 180 90 150 75
Polygon -7500403 true true 75 300 135 285 195 300
Polygon -7500403 true true 30 285 75 285 120 270 150 270 150 210 90 195 60 210 15 255
Polygon -7500403 true true 180 285 240 255 255 225 255 195 240 165 195 165 150 165 135 195 165 210 165 255

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wall-bottom
false
0
Line -7500403 true 0 300 300 300

wall-left
false
0
Line -7500403 true 0 0 0 300

wall-right
false
0
Line -7500403 true 300 0 300 300

wall-top
false
0
Line -7500403 true 0 0 300 0

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

wolf
false
0
Polygon -16777216 true false 253 133 245 131 245 133
Polygon -7500403 true true 2 194 13 197 30 191 38 193 38 205 20 226 20 257 27 265 38 266 40 260 31 253 31 230 60 206 68 198 75 209 66 228 65 243 82 261 84 268 100 267 103 261 77 239 79 231 100 207 98 196 119 201 143 202 160 195 166 210 172 213 173 238 167 251 160 248 154 265 169 264 178 247 186 240 198 260 200 271 217 271 219 262 207 258 195 230 192 198 210 184 227 164 242 144 259 145 284 151 277 141 293 140 299 134 297 127 273 119 270 105
Polygon -7500403 true true -1 195 14 180 36 166 40 153 53 140 82 131 134 133 159 126 188 115 227 108 236 102 238 98 268 86 269 92 281 87 269 103 269 113

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.2.0
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
<experiments>
  <experiment name="IndivHH_CropWarraArea" repetitions="50" runMetricsEveryStep="true">
    <setup>setup</setup>
    <go>go</go>
    <metric>hh_fsec_above_one</metric>
    <metric>hh_fsec_below_one</metric>
    <metric>hh_herd_min</metric>
    <metric>hh_without_herd</metric>
    <metric>hh_cultivating</metric>
    <metric>hh_foora</metric>
    <metric>hh_total</metric>
    <metric>food_security_mean</metric>
    <metric>herd_size_total_mean</metric>
    <metric>gini_coef_total</metric>
    <metric>gini_coef_fsec</metric>
    <metric>gini_coef_land</metric>
    <metric>cult_area_mean</metric>
    <metric>cult_area_sum</metric>
    <metric>rain</metric>
    <metric>reserve_warra</metric>
    <metric>reserve_warra_sum</metric>
    <metric>green_warra</metric>
    <metric>green_warra_sum</metric>
    <metric>warra_area_ha</metric>
    <metric>crop_area_ha</metric>
    <metric>cult_area_max_avail_ha</metric>
    <metric>cult_area_max_possible_ha</metric>
    <metric>hh_indiv_fsec</metric>
    <metric>hh_indiv_herdsize</metric>
    <metric>milk_consumption_fsec_above_one</metric>
    <metric>milk_consumption_fsec_below_one</metric>
    <metric>herd_consumption_fsec_above_one</metric>
    <metric>herd_consumption_fsec_below_one</metric>
    <metric>crop_consumption_fsec_above_one</metric>
    <metric>crop_consumption_fsec_below_one</metric>
    <metric>herd_size_total_fsec_above_one_mean</metric>
    <metric>herd_size_total_fsec_below_one_mean</metric>
    <metric>herd_size_total_fsec_above_one_sum</metric>
    <metric>herd_size_total_fsec_below_one_sum</metric>
    <metric>cult_area_fsec_above_one</metric>
    <metric>cult_area_fsec_below_one</metric>
    <metric>cult_area_fsec_above_one_sum</metric>
    <metric>cult_area_fsec_below_one_sum</metric>
    <enumeratedValueSet variable="x-steps">
      <value value="400"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="nlrx?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="fixed-seed?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="seed">
      <value value="8218"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="save-indiv-hh?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="agents-per-settlement">
      <value value="60"/>
      <value value="120"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivated-soil-types">
      <value value="&quot;all&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivated-area-max-per-hh">
      <value value="0"/>
      <value value="2"/>
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivated-area-delta-per-hh">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="crop_yield_mean">
      <value value="750"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="enclosure-size">
      <value value="80"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivation-on-enclosures?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="w">
      <value value="0.46"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m.r">
      <value value="0.13"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="m.g">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="gr1">
      <value value="0.5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="lambda">
      <value value="1.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rue">
      <value value="0.002"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reserve-max">
      <value value="150000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reserve-biomass-init">
      <value value="25000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="green-biomass-init">
      <value value="50000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-green-biomass">
      <value value="5000"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="upland-growth-rate-factor">
      <value value="0.3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="upland-reserve-max-factor">
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="upland-crop-max-factor">
      <value value="1"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="crop-prod-diff-by-soiltypes?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="same-max-cult-area-soiltypes?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivation-start-year">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproduction-rate">
      <value value="0.58"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="reproduction-rate-range">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="herdsize-init-min">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="herdsize-init-max">
      <value value="5"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-milk-herd-hh">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-milk-herd-hh">
      <value value="7"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-foora-herd-hh">
      <value value="99999"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="int_herd_size?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pastoralism-return-prob">
      <value value="0.02"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="yearly-intake">
      <value value="1600"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-mean">
      <value value="670"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-sd">
      <value value="220"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="rain-spatial-sd">
      <value value="50"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="weighted-sd?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="global-rainfall?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="households-consumption?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-per-agent">
      <value value="10"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="pop-growth-rate">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bottomlands-warra">
      <value value="0.35"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="bottomlands-foora">
      <value value="0.15"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="uplands-warra">
      <value value="0.6"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="uplands-foora">
      <value value="0.83"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="enclosure-distance">
      <value value="2"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cropland-dist-min">
      <value value="0"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cropland-dist-max">
      <value value="17"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivated-area-decrease?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="cultivated-area-constant?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="max-herd-size-poor-increase-cultivation">
      <value value="3"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="min-herd-size-rich-increase-cultivation">
      <value value="8"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="param_set">
      <value value="&quot;A&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="enclosure-outline?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="hh-below-consumption-need-die?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="patch-color-view">
      <value value="&quot;land-use&quot;"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="export-plots?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="log-messages?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="view-crop-shares?">
      <value value="true"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="crops-enclosures-barriers?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="plot-longterm?">
      <value value="false"/>
    </enumeratedValueSet>
    <enumeratedValueSet variable="view-clock?">
      <value value="false"/>
    </enumeratedValueSet>
  </experiment>
</experiments>
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
1
@#$#@#$#@
