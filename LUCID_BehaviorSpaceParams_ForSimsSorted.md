["cultivated-soil-types" "bottomland"]
["cultivation-on-enclosures?" false]
["cultivated-area-max-per-hh" 6]
["cultivated-area-delta-per-hh" 0.1]
["agents-per-settlement" 60]
["reproduction-rate" 0.4]
["rain-mean" 700]
["rain-sd" 210]
["m.r" 0.1]
["w" 0.8]
["upland-growth-rate-factor" 0.25]
["upland-reserve-max-factor" 0.5]
["enclosure-size" 65]
["x-steps" 400]
["seed?" false]
["seed" -392678159]
["hh-per-agent" 10]
["pop-growth-rate" 0]
["households-consumption?" true]
["hh-below-consumption-need-die?" false]
["weighted-sd?" true]
["rain-spatial-sd" 50]
["global-rainfall?" false]
["green-biomass-init" 50000]
["rue" 0.002]
["m.g" 0]
["gr1" 0.5]
["lambda" 1.5]
["reserve-biomass-init" 25000]
["reserve-max" 150000]
["min-green-biomass" 5000]
["upland-crop-max-factor" 1]
["reproduction-rate-range" 0]
["herdsize-init-hh-min" 10]
["herdsize-init-hh-max" 10]
["min-milk-herd-hh" 1]
["max-milk-herd-hh" 7]
["max-foora-herd-hh" 99999]
["cropland-dist-min" 0]
["cropland-dist-max" 17]
["same-max-cult-area-soiltypes?" true]
["cultivation-start-year" 10]
["cultivated-area-constant?" false]
["cultivated-area-decrease?" true]
["crop_yield_mean" 2100]
["pastoralism-return-prob" 0.02]
["enclosure-distance" 2]
["bottomlands-warra" 0.35]
["bottomlands-foora" 0.15]
["uplands-warra" 0.6]
["uplands-foora" 0.83]
["yearly-intake" 1600]
["min-herd-size-rich-increase-cultivation" 8]
["max-herd-size-poor-increase-cultivation" 3]
["crops-enclosures-barriers?" false]
["export-plots?" false]
["log-messages?" false]
["view-crop-shares?" true]
["plot-longterm?" true]
["view-clock?" false]
["patch-color-view" "green-biomass"]
["enclosure-outline?" true]
